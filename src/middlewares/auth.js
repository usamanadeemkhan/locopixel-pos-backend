const httpStatus = require('http-status');
const passport = require('passport');
const Promise = require('bluebird');
const validator = require('validator');

const APIError = require('../utils/APIError');
const Joi = require ('joi');

const generateToken = length => Math.floor((Math.random() * 989898989888) + 1000000000).toString().substr(0, length);

const handleJWT = (req, res, next) => async (err, user, info) => {
    const error = err || info;
    const logIn = Promise.promisify(req.logIn);
    const apiError = new APIError({
        message: error ? error.message : 'Unauthorized',
        status: httpStatus.UNAUTHORIZED,
        stack: error ? error.stack : undefined,
    });

    try {
        if (error || !user) throw error;
        await logIn(user, { session: false });
    } catch (e) {
        return next(apiError);
    }

    req.user = user;

    return next();
};

exports.authorize = (req, res, next) => passport.authenticate('jwt', { session: false }, handleJWT(req, res, next))(req, res, next);

exports.oAuth = service =>
    passport.authenticate(service, { session: false });

exports.preRegister = (req, res, next) => {
    req.body.resetToken = generateToken(8);
    req.body.hash = null;
    req.body.CreatedBy = req.user.id;
    next();
};

exports.preUserCreate = (req, res, next) => {
    req.body.resetToken = generateToken(8);
    req.body.hash = null;
    req.body.CreatedBy = req.user.id;
    req.body.department = 'Insurance';
    req.body.additional = '';
    next();
};

exports.mustHave = role => (req, res, next) => req.user.roles.filter(item => item.type === role).length > 0 ? next() : next(new APIError({ message: `ROLE OF '${ role }' IS REQUIRED`, status: httpStatus.UNAUTHORIZED }));

exports.preLogin = (req, res, next) => {
    if (!req.body.username ||  req.body.username.length < 5) {
        return next(new APIError({ message: 'INVALID_USERNAME', status: httpStatus.BAD_REQUEST }));
    } else if (!req.body.password || req.body.password.length < 8 ) {
        return next(new APIError({ message: 'INVALID_PASSWORD_LENGTH', status: httpStatus.BAD_REQUEST }));
    }
    if (validator.isEmail(req.body.username)) {
        req.body.email = req.body.username;
    } else {
        req.body.phone = req.body.username;
    }
    next();
};