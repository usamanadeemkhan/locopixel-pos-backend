const success = (res, message, statusCode, data) => {
    res.status(statusCode).send({ message: message, statusCode: statusCode, data: data });
};
module.exports = { success };