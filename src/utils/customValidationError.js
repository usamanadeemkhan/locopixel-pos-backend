var APIError = require("./APIError");

module.exports = (field, status, message) => {
    return new APIError({
        message: 'VALIDATION_ERROR',
        errors: [{
            field: field,
            location: 'body',
            messages: [message],
        }],
        status: status
    })
};