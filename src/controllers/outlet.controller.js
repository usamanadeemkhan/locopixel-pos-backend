const httpStatus = require('http-status');
const validationErr = require('../utils/customValidationError');
const db = require('../config/sequelize');
const {success} = require('../utils/successResponse');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const getAllOutlets = async (req, res, next) => {
    try {
        let outlets = await db.Outlet.findAll({where: {OfStore: req.params.storeId}});
        if (outlets.length !== 0) {
            success(res, 'Following are the Outlets:', httpStatus.CREATED, {"outlets": outlets});
        } else {
            success(res, 'Outlet', httpStatus.CONFLICT, 'No outlet found.');
        }
    }
    catch (error) {
        return next(error)
    }
};

const createOutlet = async (req, res, next) => {
    try {
        console.log('trying: ');
        let createdOutlet = await db.Outlet.findAll({where: {name: req.body.name, OfStore:req.params.storeId}});

        console.log('create outlet: ' + JSON.stringify(createdOutlet));
        if (createdOutlet.length !== 0) {
            console.log('in if: ');
            success(res, 'Outlet', httpStatus.CONFLICT, 'This Outlet is already exists.');
        }
        else {
            const createdOutlet = await db.Outlet.create({
                name: req.body.name,
                phone: req.body.phone,
                location: req.body.location,
                timezone: req.body.timezone,
                currency: req.body.currency,
                city: req.body.city,
                OfStore: req.params.storeId
            });

            if (createdOutlet.length !== 0) {
                    const createdOutletTax = await db.TaxOutlet.create({OfTax: req.body.tax[0].id, OfOutlet: createdOutlet.id});
                    if (createdOutletTax.length !== 0) {
                        success(res, 'New Outlet created with Tax', httpStatus.CREATED, createdOutlet);
                    } else success(res, 'New Outlet created without Tax.', httpStatus.CREATED, createdOutlet);
                } else {
                success(res, 'Error!', httpStatus.CREATED, 'Can\'t create outlet');
            }
        }
    }
    catch (error) {
        return next(error);
    }
};

const updateOutlet = async (req, res, next) => {
    try {
        let foundOutlet = await db.Outlet.findOne({where: {id: req.params.outletId}});

        if (foundOutlet.length !== 0) {
            let foundExistedOutlet = await db.Outlet.findOne({
                where: {
                    name: req.body.name,
                    id: {
                        [Op.notIn]: [req.params.outletId]
                    }}});
            if (foundExistedOutlet !== null) {
                console.log(JSON.stringify(foundExistedOutlet));
                success(res, ' Outlet with this name already existed. Choose unique name', httpStatus.CREATED, foundExistedOutlet);
            } else {
                const updatedOutlet = await db.Outlet.update(
                    req.body,
                    {where: {id: req.params.outletId}}
                );
                if (updatedOutlet.length !== 0) {
                    console.log('finding tax of outlet: ' + req.params.outletId);
                    let foundTaxOutlet = await db.TaxOutlet.findAll({where: {OfOutlet: req.params.outletId}});

                    if (foundTaxOutlet.length !== 0) {
                        console.log('found tax: OfOutlet' + foundTaxOutlet.OfOutlet + ' OfTax = ' + req.body.OfTax[0].id);
                        await db.TaxOutlet.update(
                            {OfTax: req.body.OfTax[0].id},
                            {where: {OfOutlet: req.params.outletId}}
                        );

                        console.log('found tax completed');
                    }
                    success(res, ' Outlet Updated.', httpStatus.CREATED, foundOutlet);
                }
            }
        } else {
            throw validationErr('Outlet', httpStatus.CONFLICT, 'This outlet already exist.');
        }
    }
    catch (error) {
        return next(error);
    }

};

const deleteOutLet = async (req, res, next) => {

    try {

        let foundOutlet = await db.Outlet.findAll({where: {id: req.params.outletId}});

        if (foundOutlet.length !== 0) {
            let foundTaxOutlet = await db.TaxOutlet.findAll({where: {OfOutlet: req.params.outletId}});
            if (foundTaxOutlet !== 0) {
                const deletedOutlet = await db.TaxOutlet.destroy(
                    {where: {OfOutlet: req.params.outletId}}
                );
                if (deletedOutlet === 1) {
                    await db.Outlet.destroy(
                        {where: {id: req.params.outletId}}
                    );
                    success(res, ' Outlet Deleted.', httpStatus.CREATED, foundOutlet);
                } else {
                    await db.Outlet.destroy(
                        {where: {id: req.params.outletId}}
                    );
                    success(res, ' Outlet Deleted.', httpStatus.CREATED, foundOutlet);
                }
            } else {
            }
        }
        else {
            success(res, 'Outlet', httpStatus.CONFLICT, 'Outlet doesn\'t exists.');
        }
    }
    catch (error) {
        return next(error);
    }
};

module.exports = {getAllOutlets, createOutlet, updateOutlet, deleteOutLet};