const httpStatus = require('http-status');
const validationErr = require('../utils/customValidationError');
const db = require('../config/sequelize');
const { success } = require('../utils/successResponse');
const Op = db.Sequelize.Op;
const moment = require('moment');

// CRUD Start

const getInvoices = async (req, res, next) => {
    try {
        // await db.sequelize.query("select invoices.id,\n" +
        //     "invoices.amount,\n" +
        //     "invoices.grossAmount as saleTotal,\n" +
        //     "invoices.dateOfInvoice,\n" +
        //     "invoices.discount,\n" +
        //     "invoices.OfCustomer,\n" +
        //     "users.name as employee,\n" +
        //     "products.name as \"ItemName\",\n" +
        //     "invoicedetails.OfProduct,\n" +
        //     "invoicedetails.quantity,\n" +
        //     "invoicedetails.grossAmount\n" +
        //     "from invoices\n" +
        //     "left join invoicedetails on invoices.id=invoicedetails.OfInvoice\n" +
        //     "left join products on invoicedetails.OfProduct=products.id\n" +
        //     "left join users ON invoices.OfUser =users.id\n" +
        //     "where invoices.createdAt>DATE_SUB(CURDATE(),INTERVAL 30 DAY)\n"+
        //     "and products.OfOutlet="+req.params.outletId+"  order by invoices.id").spread((results, metadata) => {
        //     success(res, ' all products .', httpStatus.OK, results);
        // });
        
        var dateBefore30Days = new moment().subtract(30, 'days').format("YYYY-MM-DD");

        var whereQuery = {
            createdAt: {[Op.gt]: dateBefore30Days}
        };

        if(req.params.outletId){
            whereQuery['$InvoiceDetails.Products.OfOutlet$'] = req.params.outletId;
        }

        await db.Invoice.findAll({
            where: whereQuery,
            attributes: ['id', 'amount', ['grossAmount', 'saleTotal'], 'dateOfInvoice', 'discount', 'OfCustomer'],
            include: [
                {
                    model: db.InvoiceDetail,
                    as: 'InvoiceDetails',
                    attributes: ['OfProduct', 'quantity', 'grossAmount'],
                    include: [
                        {
                            model: db.Product,
                            as: 'Products',
                            attributes: [['name', 'ItemName']]
                        }
                    ]
                },
                {
                    model: db.User,
                    attributes: [['name', 'employee']]
                }
            ],
            order: ['id']
        }).then((results, metadata) => {
            success(res, ' all products .', httpStatus.OK, results);
        });

    } catch (error) {
        return next(error);
    }
};

const createInvoice = async(req, res, next) => {
    try {
        
        console .log('here');
        var updatedproductscount = 0;
        if (req.body.products.length != 0) {
            let newTransaction = await db.Invoice.create({
                discount: req.body.discount,
                amount: req.body.amount,
                grossAmount: req.body.grossAmount,
                dateOfInvoice: req.body.dateOfInvoice,
                OfCustomer: req.body.OfCustomer || null,
                OfUser: req.body.OfUser
            });
            let totalcount = 0;
            let totalgoss = 0;
            let totalgossdiscount = 0;
            console.log(newTransaction);
            console.log('lenght : '+req.body.products.length);
            for (let counter = 0; counter < req.body.products.length; counter++) {console.log('i : '+counter);
                ++updatedproductscount;
                req.body.products[counter].OfInvoice = newTransaction.id;

                // await db.sequelize.query("select MAX(discounts.percentage) as percentage\n" +
                //     "from discounts\n" +
                //     "left join universaldiscounts on discounts.id=universaldiscounts.OfDiscount\n" +
                //     "left join vendorproducts on universaldiscounts.\tOfVendorProduct=vendorproducts.id\n" +
                //     "and discounts.status='Active'\n"+
                //     "where universaldiscounts.OfOutlet=\n" + req.body.OfOutlet + " " +
                //     "and universaldiscounts.OfVendorProduct=" + req.body.products[counter].OfProduct)
                    
                    await db.Discount.findAll({
                        where: {
                            status: 'Active',
                            '$UniversalDiscounts.OfOutlet$': req.body.OfOutlet,
                            '$UniversalDiscounts.OfVendorProduct$': req.body.products[counter].OfProduct
                        },
                        attributes: [[db.sequelize.fn('MAX', 'percentage'), 'percentage']],
                        include: [
                            {
                                model: db.UniversalDiscount,
                                as: 'UniversalDiscounts',
                                include: [
                                    {
                                        model: db.VendorProduct,
                                        as: 'VendorProducts'
                                    }
                                ]
                            }
                        ]
                    }).then((results, metadata) => {

                    if (results[0].percentage != null) {

                        for (let key in results) {
                            console.log(results[0].percentage);
                            let discount_amount;
                            req.body.products[counter].discount = results[key].percentage;
                            discount_amount = (req.body.products[counter].amount * (results[key].percentage / 100));
                            req.body.products[counter].grossAmount = req.body.products[counter].amount - discount_amount;
                            totalcount += req.body.products[counter].grossAmount;

                            if (req.body.discount != 0) {
                                totalgossdiscount = req.body.products[counter].grossAmount * (req.body.discount / 100);
                                totalgoss += (req.body.products[counter].grossAmount - totalgossdiscount);
                            }
                            else
                                totalgoss += req.body.products[counter].grossAmount;

                            db.VendorProduct.findById(req.body.products[counter].OfProduct).then(VendorProduct => {


                                VendorProduct.decrement('Quantity', {by: req.body.products[counter].quantity});
                            }).then(() => {
                                db.InvoiceDetail.create(req.body.products[counter]);
                            }).then(() => {
                                db.Invoice.update(
                                    {amount: totalcount, grossAmount: totalgoss},
                                    {where: {id: newTransaction.id}}
                                )
                            })

                        }


                    }
                    else {

                        req.body.products[counter].discount = 0;
                        totalcount += req.body.products[counter].grossAmount;
                        if (req.body.discount != 0) {
                            totalgossdiscount = req.body.products[counter].grossAmount * (req.body.discount / 100);
                            totalgoss += (req.body.products[counter].grossAmount - totalgossdiscount);

                        }
                        else
                            totalgoss += req.body.products[counter].grossAmount;
                    //    console.log('here');
                        db.VendorProduct.findById(req.body.products[counter].OfProduct).then(VendorProduct => {

                            //   Product.grossAmount=totalcount;

                            return VendorProduct.decrement('Quantity', {by: req.body.products[counter].quantity})
                        }).then(() => {
                            db.InvoiceDetail.create(req.body.products[counter]);

                        }).then(() => {
                            db.Invoice.update(
                                {
                                    amount: totalcount,
                                    grossAmount: totalgoss
                                },


                                {where: {id: newTransaction.id}}
                            )
                        })
                    }


                });


            }
            success(res, 'Transactions completed.', httpStatus.OK);


        }
        else
        {
            res.send("can not create transaction");
        }
    } catch (error) {
        return next(error);
    }
};

const updateInvoice = async(req, res, next) => {
    try {
        const foundInvoice = await db.Invoice.findById(req.params.invoiceId);
        if (!foundInvoice) {
            throw validationErr('invoice', httpStatus.CONFLICT, 'Invoice doesn\'t exist.');
        } else {
            const updatedInvoice = await foundInvoice.update(
                req.body
                ,{
                    where: { id: req.params.invoiceId }
                });
            success(res, 'Invoice Updated.', httpStatus.OK, updatedInvoice);
        }
    } catch (error) {
        return next(error);
    }
};
const deleteInvoice = async(req, res, next) => {
    try {
        const foundInvoice = await db.Invoice.findById(req.params.invoiceId);
        if (!foundInvoice) {
            throw validationErr('invoice', httpStatus.CONFLICT, 'Invoice doesn\'t exist.');
        } else {
            const deletedInvoice = await foundInvoice.destroy({
                where: { id: req.params.invoiceId }
            });
            success(res, 'Order deleted.', httpStatus.OK, deletedInvoice);
        }
    } catch (error) {
        return next(error);
    }
};

// CRUD End

module.exports =  { createInvoice, getInvoices, updateInvoice, deleteInvoice };