var httpStatus = require('http-status');
var validationErr = require('../utils/customValidationError');
var APIError = require("../utils/APIError");
var db = require('../config/sequelize');
var bcrypt = require('bcryptjs');
var ENV = require('../env');
var moment = require('moment-timezone');
var jwt = require('jwt-simple');
//var mailer =require( '../config/mailer');
var {success} = require('../utils/successResponse');
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({extended: false});

const getTransactions = async (req, res, next) => {
    try {
        let Transaction = await db.Invoice.findAll();

        success(res, 'All Transactions', httpStatus.OK, Transaction);
    } catch (error) {
        return next(error);
    }
};

const createTransaction = async (req, res, next) => {
    try {


        var updatedproductscount = 0;
        if (req.body.products.length != 0) {
            let newTransaction = await db.Invoice.create({
                discount: req.body.discount,
                amount: req.body.amount,
                grossAmount: req.body.grossAmount,
                dateOfInvoice: req.body.dateOfInvoice,
                OfCustomer: req.body.OfCustomer,
                OfUser: req.body.OfUser
            });
            let totalcount = 0;
            let totalgoss = 0;
            let totalgossdiscount = 0;
            console.log('lenght : '+req.body.products.length);
            for (let counter = 0; counter < req.body.products.length; counter++) {console.log('i : '+counter);
                ++updatedproductscount;
                req.body.products[counter].OfInvoice = newTransaction.id;

                // await db.sequelize.query("select discounts.percentage\n" +
                //     "from discounts\n" +
                //     "left join universaldiscounts on discounts.id=universaldiscounts.OfDiscount\n" +
                //     "left join products on universaldiscounts.OfProduct=products.id\n" +
                //     "where universaldiscounts.OfOutlet=\n" + req.body.OfOutlet + " " +
                //     "and universaldiscounts.OfProduct=" + req.body.products[counter].OfProduct).spread((results, metadata) => {
                    
                await db.Discount.findAll({
                    attributes: ['percentage'],
                    include: [
                        {
                            model: db.UniversalDiscount,
                            as: 'UniversalDiscounts',
                            include: [
                                {
                                    model: db.Product
                                }
                            ]
                        }
                    ],
                    where: {
                        '$UniversalDiscounts.OfOutlet$': req.body.OfOutlet,
                        '$UniversalDiscounts.OfProduct$': req.body.products[counter].OfProduct
                    }
                }).then((results, metadata) => {

                    if (results.length != 0) {

                        for (let key in results) {
                            let discount_amount;
                            req.body.products[counter].discount = results[key].percentage;
                            discount_amount = (req.body.products[counter].amount * (results[key].percentage / 100));
                            req.body.products[counter].grossAmount = req.body.products[counter].amount - discount_amount;
                            totalcount += req.body.products[counter].grossAmount;

                            if (req.body.discount != 0) {
                                totalgossdiscount = req.body.products[counter].grossAmount * (req.body.discount / 100);
                                totalgoss += (req.body.products[counter].grossAmount - totalgossdiscount);
                            }
                            else
                                totalgoss = req.body.products[counter].grossAmount;

                            db.Product.findById(req.body.products[counter].OfProduct).then(Product => {


                                Product.decrement('inHandQuantity', {by: req.body.products[counter].quantity});
                            }).then(() => {
                                db.InvoiceDetail.create(req.body.products[counter]);
                            }).then(() => {
                                db.Invoice.update(
                                    {amount: totalcount, grossAmount: totalgoss},
                                    {where: {id: newTransaction.id}}
                                )
                            })

                        }


                    }
                    else {
                        req.body.products[counter].discount = 0;
                        totalcount += req.body.products[counter].grossAmount;
                        if (req.body.discount != 0) {
                            totalgossdiscount = req.body.products[counter].grossAmount * (req.body.discount / 100);
                            totalgoss += (req.body.products[counter].grossAmount - totalgossdiscount);

                        }
                        else
                            totalgoss = req.body.products[counter].grossAmount;
                        db.Product.findById(req.body.products[counter].OfProduct).then(Product => {

                            //   Product.grossAmount=totalcount;

                            return Product.decrement('inHandQuantity', {by: req.body.products[counter].quantity})
                        }).then(() => {
                            db.InvoiceDetail.create(req.body.products[counter]);

                        }).then(() => {
                            db.Invoice.update(
                                {
                                    amount: totalcount,
                                    grossAmount: totalgoss
                                },


                                {where: {id: newTransaction.id}}
                            )
                        })
                    }


                });


            }
            success(res, 'Transactions completed.', httpStatus.OK);


        }
        else
        {
            res.send("can not create transaction");
        }
    }
    catch (error) {
        return next(error);
    }
};


const updateTransactions = async (req, res, next) => {
    try {
        const foundTransaction = await db.Invoice.findById(req.params.invoiceId);
        if (!foundTransactions) {
            throw validationErr('store', httpStatus.CONFLICT, 'Transactions doesn\'t exist.');
        } else {
            const updatedTransaction = await foundTransaction.update(
                req.body
                , {
                    where: {id: req.params.invoiceId}
                });
            success(res, 'Transactions Updated.', httpStatus.OK, updatedTransaction);
        }
    } catch (error) {
        return next(error);
    }
};
const deleteTransaction = async (req, res, next) => {
    try {
        const foundTransaction = await db.Invoice.findById(req.params.invoiceId);
        if (!foundTransaction) {
            throw validationErr('store', httpStatus.CONFLICT, 'Store doesn\'t exist.');
        } else {
            const deletedTransaction = await foundTransaction.destroy({
                where: {id: req.params.invoiceId}
            });
            success(res, 'Invoice deleted.', httpStatus.OK, deletedTransaction);
        }
    } catch (error) {
        return next(error);
    }
};

// CRUD End

module.exports = {getTransactions, createTransaction, updateTransactions, deleteTransaction};