const httpStatus = require('http-status');
const validationErr = require('../utils/customValidationError');
const db = require('../config/sequelize');
const { success } = require('../utils/successResponse');

// CRUD Start

const getTaxes = async (req, res, next) => {
    try {
        let foundTax = await db.Tax.findAll();
        if (foundTax.length !== 0) {
            success(res, 'Taxes are: ', httpStatus.OK, foundTax);
        } else {
            success(res, 'Error', httpStatus.OK, 'No tax exists');
        }
        // console.log('Users: \n' + JSON.stringify(users));

    } catch (error) {
        return next(error);
    }
};
const getOutletTaxes = async (req, res, next) => {
    try {
        let taxOutlets = await db.TaxOutlet.findAll({where: {OfOutlet : req.params.taxOutletId}});
        if (taxOutlets.length !== 0) {
            // console.log('Tax: ' + JSON.stringify(taxOutlets));
            success(res, 'Taxes are: ', httpStatus.OK, taxOutlets);
        } else {
            success(res, 'Error', httpStatus.OK, 'No tax exists');
        }
        // console.log('Users: \n' + JSON.stringify(users));

    } catch (error) {
        return next(error);
    }
};
const getSpecificTax = async (req, res, next) => {
    try {
        db.Tax.findAll({
            attributes: ['name', 'rate'],
            include: [
                {
                    model: db.TaxOutlet,
                    as: 'TexOutlets'
                }
            ],
            where: {
                '$TexOutlets.OfOutlet$': req.params.outletId
            }
        }).then((results, metadata) => {
            success(res, ' all products .', httpStatus.OK, results);
        });

        // db.sequelize.query("SELECT taxes.name,taxes.rate FROM taxes\n" +
        //     "LEFT JOIN taxoutlets on taxes.id =taxoutlets.OfTax\n" +
        //     "where taxoutlets.OfOutlet =" + req.params.outletId + "").spread((results, metadata) => {
        //     success(res, ' all products .', httpStatus.OK, results);

        // });
    }catch (error) {
        return next(error);
    }
};

const createTax = async(req, res, next) => {
    try {
        const foundTax = await db.Tax.findAll({ where: {name : req.body.name}});
        if (foundTax.length !== 0) {
            throw validationErr('Tax', httpStatus.CONFLICT, 'This Tax already exists.');
        } else {
            const createdTax = await db.Tax.create(req.body);
            if (createdTax.length !== 0) {
                success(res, 'New Tax Created.', httpStatus.CREATED, createdTax);
            } else {
                // throw validationErr('Tax', httpStatus.BAD_REQUEST, 'Can not create tax.');
            }
        }
    } catch (error) {
        return next(error);
    }
};

/*
const updateStore = async(req, res, next) => {
    try {
        const foundStore = await db.Store.findById(req.params.storeId);
        if (!foundStore) {
            throw validationErr('store', httpStatus.CONFLICT, 'Store doesn\'t exist.');
        } else {
            const updatedtedStore = await foundStore.update(
                req.body
                ,{
                    where: { id: req.params.storeId }
                });
            success(res, 'Store Updated.', httpStatus.OK, updatedtedStore);
        }
    } catch (error) {
        return next(error);
    }
};
const deleteStore = async(req, res, next) => {
    try {
        const foundStore = await db.Store.findById(req.params.storeId);
        if (!foundStore) {
            throw validationErr('store', httpStatus.CONFLICT, 'Store doesn\'t exist.');
        } else {
            const deletedStore = await foundStore.destroy({
                where: { id: req.params.storeId }
            });
            success(res, 'Store deleted.', httpStatus.OK, deletedStore);
        }
    } catch (error) {
        return next(error);
    }
};
*/

// CRUD End

// module.exports =  { createStore, getStores, updateStore, deleteStore };
module.exports =  { getTaxes, createTax, getOutletTaxes ,getSpecificTax };