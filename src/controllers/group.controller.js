const httpStatus = require('http-status');
const validationErr = require('../utils/customValidationError');
const db = require('../config/sequelize');
const {success} = require('../utils/successResponse');

const getAllGroups = async (req, res, next) => {
    try {
        await db.Group.findAll({
            attributes: ['id', ['name', 'group_name'], 'description', 'groupType', ['createdAt', 'Date_Created']],
            include: [
                {
                    model: db.GroupMember,
                    as: 'GroupMembers',
                    include: [
                        {
                            model: db.User,
                            attributes: [[db.sequelize.fn('COUNT', 'id'), 'total_users']]
                        }
                    ]
                },
                {
                    model: db.OutletGroup,
                    as: 'OutletGroups',
                    include: [
                        {
                            model: db.Outlet,
                            attributes: [['name', 'outlet']]
                        }
                    ]
                }
                
            ],
            group: ['id'],
            order: ['name']
        }).then((results) => {
            success(res, 'All Groups', httpStatus.OK, results);
        });

        // await db.sequelize.query("SELECT groups.id, groups.description, groups.name as group_name, groups.groupType, outlets.name as outlet, groups.createdAt as Date_Created, count(users.id) as total_users " +
        //     "FROM groups " +
        //     "LEFT JOIN groupmembers ON groups.id = groupmembers.OfGroup " +   
        //     "LEFT JOIN users ON groupmembers.OfUser = users.id " +
        //     "LEFT JOIN outletgroups ON  groups.id = outletgroups.OfGroup " +
        //     "LEFT JOIN outlets ON outletgroups.OfOutlet = outlets.id " + 
        //     "GROUP BY groups.id " +
        //     "ORDER BY groups.name").spread((results, metadata) => {
        //     success(res, 'All Groups', httpStatus.OK, results);
        //     // Results will be an empty array and metadata will contain the number of affected rows.
        // })

    }
    catch (error) {
        return next(error);
    }
};

const createGroup = async (req, res, next) => {
    try {
        let foundGroup = await db.Group.findAll({where: {name: req.body.name}});
        if (foundGroup.length !== 0) {
            throw validationErr('group', httpStatus.CONFLICT, 'This Group already Exists.');
        } else {
            const createdGroup = await db.Group.create(req.body);

            if (createdGroup) {
                const customerLength = (req.body.customer.length - 1);
                let customerCount = 0;

                while (customerCount <= customerLength) {
                    let addedCustomer = await db.GroupMember.create({
                        OfGroup: createdGroup.id,
                        OfUser: req.body.customer[customerCount].id
                    });
                    if (addedCustomer) {
                        //success(res, 'Outlet Assigned.', httpStatus.CREATED, assignedOutlet);
                    } else {
                        throw validationErr('Customer', httpStatus.CONFLICT, 'Customer not added.');
                    }
                    customerCount++;
                }

                const outletLength = (req.body.outlet.length - 1);
                let outletCount = 0;

                while (outletCount <= outletLength) {
                    //console.log("inside loop");
                    let assignedOutlet = await db.OutletGroup.create({
                        OfGroup: createdGroup.id,
                        OfOutlet: req.body.outlet[outletCount].id
                    });
                    console.log("Assigned outlet");
                    if (assignedOutlet) {
                        //success(res, 'Outlet Assigned.', httpStatus.CREATED, assignedOutlet);
                    } else {
                        throw validationErr('Outlet', httpStatus.CONFLICT, 'Outlet not assigned.');
                    }
                    outletCount++;
                }
                success(res, 'New Group Added.', httpStatus.CREATED, createdGroup);
            } else {
                throw validationErr('Group', httpStatus.CONFLICT, 'Group not created.');
            }
        }
    }
    catch (error) {
        return next(error);
    }

};

const updateGroup = async (req, res, next) => {

    try {

        let foundGroup;

        foundGroup = await db.Group.findAll({where: {id: req.params.groupId}});
        console.log('Group Found: ' + foundGroup);
        if (foundGroup.length !== 0) {
            const UpdatedGroup = await db.Group.update(
                req.body,
                {where: {id: req.params.groupId}}
            );
            success(res, ' Group Updated.', httpStatus.CREATED, UpdatedGroup);
        }
        else {
            throw validationErr('Group', httpStatus.CONFLICT, 'This name already exist.');

        }
    }
    catch (error) {
        return next(error);
    }
};

const deleteGroup = async (req, res, next) => {

    try {

        let delGroup;

        delGroup = await db.Group.findAll({where: {id: req.params.groupId}});

        if (delGroup.length != 0) {
            const deleteGroup = await db.Group.destroy(
                {where: {id: req.params.groupId}}
            );
            success(res, 'Group Deleted.', httpStatus.CREATED, deleteGroup);
        }
        else {
            throw validationErr('Group', httpStatus.CONFLICT, 'No such Group.');

        }
    }
    catch (error) {
        return next(error);
    }
};

module.exports = {getAllGroups, createGroup, updateGroup, deleteGroup};