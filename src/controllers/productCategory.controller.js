const httpStatus = require('http-status');
const validationErr = require('../utils/customValidationError');
const db = require('../config/sequelize');
const {success} = require('../utils/successResponse');

// CRUD Start

const getCategory = async (req, res, next) => {
    try {

        let categories = await db.Category.findAll();
        // console.log('Users: \n' + JSON.stringify(users));
        success(res, 'All Product Categories', httpStatus.OK, categories);
    } catch (error) {
        return next(error);
    }
};

const createCategory = async (req, res, next) => {
    try {
        const createdCategory = await db.Category.create(req.body);
        success(res, 'New Store Created.', httpStatus.CREATED, createdCategory);
    } catch (error) {
        return next(error);
    }
};

const updateStore = async (req, res, next) => {
    try {
        const foundCategory = await db.Category.findById(req.params.categoryId);
        if (!foundCategory) {
            throw validationErr('store', httpStatus.CONFLICT, 'Store doesn\'t exist.');
        } else {
            const updatedtedCategory = await foundCategory.update(
                req.body
                , {
                    where: {id: req.params.categoryId}
                });
            success(res, 'Store Updated.', httpStatus.OK, updatedtedCategory);
        }
    } catch (error) {
        return next(error);
    }
};
const deleteStore = async (req, res, next) => {
    try {
        const foundCategory = await db.Category.findById(req.params.categoryId);
        if (!foundCategory) {
            throw validationErr('store', httpStatus.CONFLICT, 'Store doesn\'t exist.');
        } else {
            const deletedCategory = await foundCategory.destroy({
                where: {id: req.params.categoryId}
            });
            success(res, 'Store deleted.', httpStatus.OK, deletedCategory);
        }
    } catch (error) {
        return next(error);
    }
};
const getCategoryinOutlet = async (req, res, next) => {
    try {
        console.log("here");
        await db.Category.findAll({
            attributes: [db.sequelize.fn('DISTINCT', 'name')],
            where: {
                OfOutlet: req.params.outletId
            }
        }).then((results) => {
            success(res, 'Alls Categories of outlet', httpStatus.OK, results);
        });

        // await db.sequelize.query("  SELECT DISTINCT categories.*\n" +
        //     "                       FROM categories \n" +
        //     "                        WHERE categories.OfOutlet= " + req.params.outletId + "  ").spread((results) => {
        //     success(res, 'Alls Categories of outlet', httpStatus.OK, results);
        // });
    }
    catch (error) {
        return next(error);
    }
};
// CRUD End

module.exports = {createCategory, getCategory, updateStore, deleteStore, getCategoryinOutlet};