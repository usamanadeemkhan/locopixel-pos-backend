const httpStatus = require('http-status');
const validationErr = require('../utils/customValidationError');
const db = require('../config/sequelize');
const { success } = require('../utils/successResponse');

// CRUD Start

const getRoles = async (req, res, next) => {
    try {
        let roles = await db.UserRole.findAll();
        // console.log('Users: \n' + JSON.stringify(users));
        success(res, 'All User Roles', httpStatus.OK, roles);
    } catch (error) {
        return next(error);
    }
};

const createRole = async(req, res, next) => {
    try {
        //console.log(req.body.permissions[0]);
        let newUserRoles = await db.UserRole.findAll({ where: {roleName: req.body.roleName }});
        if (newUserRoles.length != 0 ) {
            throw validationErr('role', httpStatus.CONFLICT, 'This User role already exists.');
        }
        else{
            console.log('here');
            const createdUserRoles = await db.UserRole.create({roleName: req.body.roleName, roleDescription: req.body.roleDescription});
            if ( createdUserRoles ) {
                //success(res, 'New Role Added.', httpStatus.CREATED, createdUserRoles);
                if ( req.body.permissions.length != 0 ) {
                    let i = req.body.permissions.length-1;
                    let j = 0;
                    while (j != i) {
                        const createdPermission = await db.Permission.create(req.body.permissions[j]);
                        if ( createdPermission ) {
                            //success(res, 'New Permission Added.', httpStatus.CREATED, createdPermission);
                            const AssignedPermission = await db.RolePermission.create({ OfPermission:createdPermission.id, OfUserRole:createdUserRoles.id });
                            if ( AssignedPermission ) {
                                success(res, 'Task Completed', httpStatus.CREATED, AssignedPermission);
                            } else {
                                console.log('Task not completed');
                            }
                        } else {
                            console.log('Permission not created');
                        }
                        ++j;
                    }
                } else {
                    success(res, 'New Role Added.', httpStatus.CREATED, createdUserRoles);
                }
            }
        }
    }
    catch (error) {
        return next(error);
    }
};

const updateRole = async(req, res, next) => {
    try {
        const foundRole = await db.UserRole.findById(req.params.roleId);
        if (!foundRole) {
            throw validationErr('user role', httpStatus.CONFLICT, 'User Role doesn\'t exist.');
        } else {
            const updatedRole = await foundRole.update(
                req.body
                ,{
                    where: { id: req.params.roleId }
                });
            success(res, 'Role Updated.', httpStatus.OK, updatedRole);
        }
    } catch (error) {
        return next(error);
    }
};
const deleteRole = async(req, res, next) => {
    try {
        const foundRole = await db.Invoice.findById(req.params.roleId);
        if (!foundRole) {
            throw validationErr('role', httpStatus.CONFLICT, 'Role doesn\'t exist.');
        } else {
            const deletedInvoice = await foundRole.destroy({
                where: { id: req.params.roleId }
            });
            success(res, 'Order deleted.', httpStatus.OK, deletedInvoice);
        }
    } catch (error) {
        return next(error);
    }
};

// CRUD End

module.exports =  { createRole, getRoles, updateRole, deleteRole};