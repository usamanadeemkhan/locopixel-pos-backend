var httpStatus = require('http-status');
var validationErr = require('../utils/customValidationError');
var db = require('../config/sequelize');
const {success} = require('../utils/successResponse');

const getAllProducts = async (req, res, next) => {
    try {
        let products;
        let getTags;
        db.Product.findAll({
            attributes: ['id', 'name', 'description', 'supplyPrice', 'retailPrice', 'markup', 'totalPrice', 'productType', 'barcode', 'image', 'sku', 'batchNumber', 'inHandQuantity', 'minimumQuantity', 'expiryDate', 'createdAt'],
            include: [
                {
                    model: db.VendorProduct,
                    as: 'VendorProducts',
                    attributes: [['id', 'uniqueId'], ['SellingPrice', 'varientprice'], 'Quantity'],
                    include: [
                        {
                            model: db.Vendor,
                            attributes: [['id', 'vendorid'], ['name', 'vendorname']]
                        },
                        {
                            model: db.Variant,
                            attributes: [['id', 'varientid'], ['name', 'varientname']]
                        }
                    ]
                },
                {
                    model: db.Brand,
                    attributes: [['id', 'brandId'], ['name', 'brandname']]
                },
                {
                    model: db.Category,
                    attributes: [['id', 'CategoryId'], ['name', 'Categoryname']]
                }
            ]
        }).then((results, metadata) => {
            success(res, ' all products .', httpStatus.OK, results);
        });

        // db.sequelize.query("SELECT products.id,vp.id as uniqueId,products.name,products.description,products.supplyPrice,products.retailPrice," +
        //     "products.markup,products.totalPrice,products.productType,products.barcode,products.image,\n" +
        //     "products.sku,products.batchNumber,products.inHandQuantity,products.minimumQuantity,products.expiryDate," +
        //     "products.createdAt,vendors.id as \"vendorid\",vendors.name as \"vendorname\",brands.id as \"brand id\"," +
        //     "brands.name as \"brandname\",categories.id as \"Category id\",categories.name as \"Categoryname\"," +
        //     "vp.SellingPrice as \"varientprice\",variants.id as \"varientid\",variants.name as \"varientname\",vp.Quantity \"Quantity\"\n" +
        //     "FROM products\n" +
        //     "LEFT JOIN vendorproducts vp ON products.id=vp.OfProduct\n" +
        //     "LEFT JOIN vendors ON vp.OfVendor=vendors.id\n" +
        //     "LEFT JOIN brands on products.OfBrand=brands.id\n" +
        //     "LEFT JOIN categories on products.OfCategory=categories.id\n" +
        //     "LEFT JOIN variants ON vp.OfVarient=variants.id").spread((results, metadata) => {
        //     success(res, ' all products .', httpStatus.OK, results);
        // });


    }
    catch (error) {
        return next(error);
    }


};
const getAllProductsofOutlets = async (req, res, next) => {
    try {
        let products;
        let getTags;
        db.Product.findAll({
            attributes: ['id', 'name', 'description', 'supplyPrice', 'retailPrice', 'markup', 'totalPrice', 'productType', 'barcode', 'image', 'sku', 'batchNumber', 'inHandQuantity', 'minimumQuantity', 'expiryDate', 'createdAt'],
            include: [
                {
                    model: db.VendorProduct,
                    as: 'VendorProducts',
                    attributes: [['id', 'uniqueId'], ['SellingPrice', 'varientprice'], 'Quantity'],
                    include: [
                        {
                            model: db.Vendor,
                            attributes: [['id', 'vendorid'], ['name', 'vendorname']]
                        },
                        {
                            model: db.Variant,
                            attributes: [['id', 'varientid'], ['name', 'varientname'], ['sku', 'variantsku']]
                        }
                    ]
                },
                {
                    model: db.Brand,
                    attributes: [['id', 'brandId'], ['name', 'brandname']]
                },
                {
                    model: db.Category,
                    attributes: [['id', 'CategoryId'], ['name', 'Categoryname']]
                }
            ],
            where: {
                OfOutlet: req.params.outletId
            }
        }).then((results, metadata) => {
            success(res, 'all products .', httpStatus.OK, results);
        });

        // db.sequelize.query("SELECT products.id,vp.id as uniqueId,products.name,products.description,products.supplyPrice,products.retailPrice," +
        //     "products.markup,products.totalPrice,products.productType,products.barcode,products.image,\n" +
        //     "products.sku,products.batchNumber,products.inHandQuantity,products.minimumQuantity,products.expiryDate," +
        //     "products.createdAt,vendors.id as \"vendorId\",vendors.name as \"vendorname\",brands.id as \"brandId\"," +
        //     "brands.name as \"brandname\",categories.id as \"CategoryId\",categories.name as \"Categoryname\",variants.sku as \"variantsku\"," +
        //     "variants.id as \"varientid\",variants.name as \"varientname\",vp.SellingPrice as \"varientprice\",vp.Quantity \"Quantity\"\n" +
        //     "FROM products\n" +
        //     "LEFT JOIN vendorproducts vp ON products.id=vp.OfProduct\n" +
        //     "LEFT JOIN vendors ON vp.OfVendor=vendors.id\n" +
        //     "LEFT JOIN brands on products.OfBrand=brands.id\n" +
        //     "LEFT JOIN categories on products.OfCategory=categories.id\n" +
        //     "LEFT JOIN variants ON vp.OfVarient=variants.id"+
        //     "            WHERE products.OfOutlet=" + req.params.outletId + "").spread((results, metadata) => {
        //     success(res, ' all products .', httpStatus.OK, results);
        // });


    }
    catch (error) {
        return next(error);
    }


};

const getallproductsforoutletbyVendors = async (req, res, next) => {
    try {
        let products;
        let getTags;
        db.Product.findAll({
            attributes: ['id', 'name', 'inHandQuantity', 'minimumQuantity', 'totalPrice'],
            include: [
                {
                    model: db.VendorProduct,
                    as: 'VendorProducts',
                    attributes: [['OfVendor', 'vendorsId'], ['supplierPrice', 'supplierPrice']]
                }
            ],
            where: {
                OfOutlet: req.params.outletId,
                '$VendorProducts.OfVendor$': req.params.vendorId
            }
        }).then((results, metadata) => {
            success(res, ' all products .', httpStatus.OK, results);
        });

        // db.sequelize.query("SELECT products.id,products.name,products.inHandQuantity,products.minimumQuantity,products.totalPrice ,vendorproducts.OfVendor as vendorsId,\n" +
        //     "vendorproducts.supplierPrice as supplierPrice FROM `products` LEFT JOIN vendorproducts on products.id=vendorproducts.OfProduct\n" +
        //     "where vendorproducts.OfVendor=" + req.params.vendorId + "\n" +
        //     "AND products.OfOutlet=" + req.params.outletId + "").spread((results, metadata) => {
        //     success(res, ' all products .', httpStatus.OK, results);
        // });


    }
    catch (error) {
        return next(error);
    }


};

const createProduct = async (req, res, next) => {

    try {

        let findProduct;
        let newProduct;
        let assignvendor
        findProduct = await db.Product.findAll({where: {name: req.body.name}});

        if (findProduct.length != 0) {

            throw validationErr('Product', httpStatus.CONFLICT, 'This Product is already registered.');
        }
        else {
            newProduct = await db.Product.create({
                name: req.body.name, description: req.body.description,
                supplyPrice: req.body.supplyPrice, retailPrice: req.body.retailPrice, markup: req.body.markup,
                totalPrice: req.body.totalPrice, productType: req.body.productType, barcode: req.body.barcode,
                image: req.body.image, sku: req.body.sku, batchNumber: req.body.batchNumber,
                inHandQuantity: req.body.inHandQuantity, minimumQuantity: req.body.minimumQuantity,
                expiryDate: req.body.expiryDate, OfBrand: req.body.brandId, OfCategory: req.body.categoryId,
                OfOutlet: req.body.outletId, OfUser: req.body.userId
            });


            let findingVendor = await db.Vendor.findAll({where: {id: req.body.supplierId}});

            if (findingVendor.length != 0)                                           //create vendor if it doesnt exist
            {
                 assignvendor = await db.VendorProduct.create({
                    OfProduct: newProduct.id, OfVendor: findingVendor[0].id,
                    supplierPrice: req.body.supplyPrice,
                     Quantity:req.body.inHandQuantity,
                     SellingPrice:req.body.totalPrice
                });

            }

                if (req.body.Tags.length != 0) {
                    let tagsCount = req.body.Tags.length;
                    let mycounter = 0;
                    let producttagcount = 0;
                    while (mycounter < tagsCount) {
                        var findTag;
                        for (let key in req.body.Tags[mycounter]) {
                            findTag = await db.Tag.findAll({where: {name: req.body.Tags[mycounter][key]}});

                            if (findTag.length == 0) {
                                console.log(req.body.Tags[mycounter][key]);
                                let createTag = await  db.Tag.create({name: req.body.Tags[mycounter][key]});
                                let assignTag = await db.ProductTag.create({
                                    OfProduct: newProduct.id,
                                    OfTag: createTag.id
                                });
                            }

                            else {

                                console.log(findTag[producttagcount].id);

                                let assignTag = await db.ProductTag.create({
                                    OfProduct: newProduct.id,
                                    OfTag: findTag[producttagcount].id
                                });
                                producttagcount;
                            }

                        }

                        mycounter++;
                    }

                }


            success(res, ' Product Created.', httpStatus.CREATED, newProduct);
        }


    }
    catch (error) {
        return next(error);
    }

};


const updateProduct = async (req, res, next) => {

    try {

        let updtdProduct;
        updtdProduct = await db.Outlet.findAll({where: {id: req.params.productId}});

        if (updtdProduct.length != 0) {
            const UpdatedProduct = await db.Product.update(
                req.body,
                {where: {id: req.params.productId}}
            );
            success(res, ' Product Updated.', httpStatus.CREATED, UpdatedProduct);
        }
        else {
            throw validationErr('email', httpStatus.CONFLICT, 'This name already exist.');

        }

    }
    catch (error) {
    }

};


const deleteProduct = async (req, res, next) => {

    try {

        let detProduct;

        detProduct = await db.Outlet.findAll({where: {id: req.params.productId}});

        if (detProduct.length != 0) {
            const deletedProduct = await db.Product.destroy(
                {where: {id: req.params.productId}}
            );
            success(res, ' Outlet Deleted.', httpStatus.CREATED, deletedProduct);

        }
        else {
            throw validationErr('email', httpStatus.CONFLICT, 'No such Outlet.');

        }

    }
    catch (error) {
    }
};

const getAllProductsforDiscount = async (req, res, next) => {
    try {

        let products;
        let getTags;
        db.VendorProduct.findAll({
            attributes: [['id', 'uniqueId'], ['Quantity', 'variantQuantity'], ['supplierPrice', 'variantprice'], ['SellingPrice', 'variantprice']],
            include: [
                {
                    model: db.Product,
                    as: 'Products',
                    attributes: ['id', 'name', 'description', 'supplyPrice', 'retailPrice', 'markup', 'totalPrice', 'productType', 'barcode', 'image', 'sku', 'batchNumber', 'inHandQuantity', 'minimumQuantity', 'expiryDate', 'createdAt'],
                    include: [
                        {
                            model:db.Brand,
                            attributes: [['id', 'brandId'], ['name', 'brandname']]
                        },
                        {
                            model: db.Category,
                            attributes: [['id', 'CategoryId'], ['name', 'CategoryName']]
                        }
                    ]
                },
                {
                    model: db.Vendor,
                    attributes: [['id', 'vendorId'], ['name', 'vendorname']]
                },
                {
                    model: db.Variant,
                    attributes: [['name', 'variantname'], ['sku', 'variantsku']]
                }
            ],
            where: {
                '$Products.OfOutlet$': req.params.outletId
            }
        }).then((results, metadata) => {
            success(res, ' all of products .', httpStatus.OK, results);
        });

        // db.sequelize.query("SELECT products.id,vendorproducts.id as uniqueId,products.name,products.description,products.supplyPrice,products.retailPrice," +
        //     "products.markup,products.totalPrice,products.productType,products.barcode,products.image,\n" +
        //     "products.sku,products.batchNumber,products.inHandQuantity,products.minimumQuantity,products.expiryDate," +
        //     "products.createdAt,vendors.id as \"vendorId\",variants.name as \"variantname\"," +
        //     "vendorproducts.Quantity as \"variantQuantity\",vendorproducts.supplierPrice as \"variantprice\",variants.sku as \"variantsku\"" +
        //     ",vendorproducts.SellingPrice as \"variantprice\",vendors.name as \"vendorname\",brands.id as \"brand id\"," +
        //     "brands.name as \"brandname\",categories.id as \"Category id\",categories.name as \"Categoryname\"" +
        //     "            FROM vendorproducts \n" +
        //     "            LEFT JOIN products  ON vendorproducts.OfProduct=products.id\n" +
        //     "            LEFT JOIN variants  ON vendorproducts.OfVarient=variants.id\n" +
        //     "            LEFT JOIN vendors ON vendorproducts.OfVendor=vendors.id\n" +
        //     "            LEFT JOIN brands on products.OfBrand=brands.id\n" +
        //     "            LEFT JOIN categories on products.OfCategory=categories.id\n" +
        //     "            WHERE products.OfOutlet=" + req.params.outletId + "").spread((results, metadata) => {
        //     success(res, ' all of products .', httpStatus.OK, results);
        // });

    }
    catch (error) {
        return next(error);
    }


};


module.exports = {
    getAllProducts,
    createProduct,
    updateProduct,
    deleteProduct, getAllProductsforDiscount,
    getAllProductsofOutlets,
    getallproductsforoutletbyVendors
};