const httpStatus = require('http-status');
const validationErr = require('../utils/customValidationError');
const APIError = require("../utils/APIError");
const db = require('../config/sequelize');
const bcrypt = require('bcryptjs');
const ENV = require('../env');
const moment = require('moment-timezone');
const jwt = require('jwt-simple');
const {success} = require('../utils/successResponse');

const token = (roles, user) => {
    const playload = {
        exp: moment().add(ENV['JWT_EXPIRATION_MINUTES'], 'minutes').unix(),
        iat: moment().unix(),
        id: user.id,
        roles: roles,
        name: user.name,
        email: user.email,
        address: user.address,
        country: user.country,
        city: user.city
    };
    return jwt.encode(playload, ENV['jwtSecret']);
};

const generateTokenResponse = (accessToken) => {
    const tokenType = 'Bearer';
    const expiresIn = moment().add(ENV['JWT_EXPIRATION_MINUTES'], 'minutes');
    return {tokenType, accessToken, expiresIn};
};

const passwordMatches = (password, hash) => {
    return bcrypt.compareSync(password, hash);
};

const resetPassword = async (req, res, next) => {
    try {
        const foundUser = await db.User.findOne({where: {email: req.body.email}});
        if (!foundUser) throw validationErr('email', httpStatus.METHOD_NOT_ALLOWED, 'Email not registered yet.');
        if (!foundUser.resetToken) throw new APIError({
            message: 'Cannot reset email no valid token found',
            status: httpStatus.METHOD_NOT_ALLOWED
        });
        if (foundUser.resetToken === req.body.resetToken) {
            await foundUser.update({resetToken: null, hash: await bcrypt.hash(req.body.password, ENV['CRYPT_ROUNDS'])});
            const userToken = token(await db.UserRole.findAll({where: {OfUserRole: foundUser.id}}), foundUser);
            success(res, 'Password reset successful', httpStatus.CREATED, generateTokenResponse(userToken));
        } else throw new APIError({message: 'Not a valid reset token', status: httpStatus.METHOD_NOT_ALLOWED});
    } catch (error) {
        return next(error);
    }
};

// CRUD for Employees Start

const getAllUsers = async (req, res, next) => {
    try {
        await db.User.findAll({
            attributes: ['id', 'name', 'email', 'phoneNumber', 'address'],
            include: [
                {
                    model: db.UserRole,
                    as: 'UserRoles',
                    include: [
                        {
                            model: db.Role,
                            attributes: [['name', 'role']]
                        }
                    ]
                },
                {
                    model: db.OutletUser,
                    as: 'OutletUsers',
                    include: [
                        {
                            model: db.Outlet,
                            attributes: [['name', 'outlet']]
                        }
                    ]
                }
            ],
            where: {
                isCustomer: false,
                isAdmin: !true
            }
        }).then((results, metadata) => {
            success(res, 'All Users', httpStatus.OK, results);
        });

        // await db.sequelize.query("SELECT users.id, users.name, users.email, users.phoneNumber, users.address, roles.name as role, outlets.name as outlet\n" +
        //     "                     FROM users \n" +
        //     "                      LEFT JOIN userroles ON users.id = userroles.OfUser\n" +
        //     "                     LEFT JOIN roles ON userroles.OfRole = roles.id\n" +
        //     "                     LEFT JOIN outletusers ON users.id = outletusers.OfUser\n" +

        //     "                     LEFT JOIN outlets ON outletusers.OfOutlet = outlets.id WHERE users.isCustomer= false AND users.isAdmin != true").spread((results, metadata) => {

        //     success(res, 'All Users', httpStatus.OK, results);
        //     // Results will be an empty array and metadata will contain the number of affected rows.
        // })
    } catch (error) {
        return next(error);
    }
};

// Add New User and Assign Outlet(s) then Assign Role(s)
// *** Create User ***
/* Getting new user's info in body.
   First check whether user already exists or not
   Then create user with the info sent in the body
   If user created successfully then check if outlet(s) is/are selected, if yes then receive outlet(s) in the form of array of objects

    */
const createUser = async (req, res, next) => {
    try {
        const foundUser = await db.User.findOne({where: {email: req.body.email}});
        if (foundUser) {
            success(res, 'User', httpStatus.CONFLICT, 'User already exists.');
        } else {
            const createdUser = await db.User.create(req.body);
            if (createdUser) {
                //console.log("user created");
                const outletLength = (req.body.outlet.length - 1);
                let outletCount = 0;
                while (outletCount <= outletLength) {
                    // console.log("inside loop");
                    let assignedOutlet = await db.OutletUser.create({
                        OfUser: createdUser.id,
                        OfOutlet: req.body.outlet[outletCount].id
                    });
                    console.log("Assigned outlet");
                    if (assignedOutlet) {
                        //success(res, 'Outlet Assigned.', httpStatus.CREATED, assignedOutlet);
                    } else {
                        throw validationErr('Outlet', httpStatus.CONFLICT, 'Outlet not assigned.');
                    }
                    outletCount++;
                }
                let roleLength = (req.body.role.length - 1);
                let roleCount = 0;
                console.log("New Loop starting");
                while (roleCount <= roleLength) {
                    let assignedRole = await db.UserRole.create({
                        OfUser: createdUser.id,
                        OfRole: req.body.role[roleCount].id
                    });
                    if (assignedRole) {
                        //success(res, 'Role Assigned.', httpStatus.CREATED, assignedRole);
                        console.log("Assigned Role");
                    } else {
                        throw validationErr('Role', httpStatus.CONFLICT, 'Role not assigned.');
                    }
                    roleCount++;
                }
                success(res, 'User Added.', httpStatus.CREATED, createdUser);
            } else {
                throw validationErr('user', httpStatus.CONFLICT, 'User not created.');
            }
        }
    } catch (error) {
        return next(error);
    }
};

const updateUser = async (req, res, next) => {
    try {
        const foundUser = await db.User.findById(req.params.userId);
        if (!foundUser) {
            success(res, 'user', httpStatus.CONFLICT, 'User doesn\'t exist.');
        } else {
            const updatedUser = await foundUser.update(
                req.body
                , {
                    where: {id: req.params.userId}
                });
            await db.OutletUser.update(
                {OfOutlet: req.body.outlet}
                , {
                    where: {OfUser: req.params.userId}
                });
            success(res, 'User Updated.', httpStatus.OK, updatedUser);
        }
    } catch (error) {
        return next(error);
    }
};

const deleteUser = async (req, res, next) => {
    try {
        const foundUser = await db.User.findById(req.params.userId);
        if (!foundUser) {
            throw validationErr('user', httpStatus.CONFLICT, 'User doesn\'t exist.');
        } else {
            const deletedUser = await foundUser.destroy({
                where: {id: req.params.userId}
            });
            success(res, 'User deleted.', httpStatus.OK, deletedUser);
        }
    } catch (error) {
        return next(error);
    }
};

// CRUD End

const login = async (req, res, next) => {
    try {
        const foundUser = await db.User.findOne({where: {email: req.body.email}});
        if (!foundUser) success(res, 'email', httpStatus.UNAUTHORIZED, 'Email not registered yet.');
        //if (foundUser.resetToken) success(res, 'email', httpStatus.UNAUTHORIZED, 'Email not confirmed yet. Reset using email.');
        console.log("body pass: " + req.body.password);
        console.log("user pass: " + foundUser.password);
        if (toString(bcrypt.hash(req.body.password)) === toString(foundUser.password)) {
            const userToken = token(await db.UserRole.findAll({where: {OfUser: foundUser.id}}), foundUser);
            success(res, 'User Logged In Successfully', httpStatus.CREATED, generateTokenResponse(userToken));
        } else {
            success(res, 'password', httpStatus.UNAUTHORIZED, 'Password is not valid.');
        }
    } catch (error) {
        return next(error);
    }
};

// CRUD for Customers Start
const getAllCustomers = async (req, res, next) => {
    try {
        await db.User.findAll({
            attributes: ['id', 'name', 'email', 'dateOfBirth', 'phoneNumber', 'gender', 'address', 'orderTotal', 'spentTotal', 'visitTotal', 'isCustomer'],
            include: [
                {
                    model: db.OutletUser,
                    as: 'OutletUsers',
                    include: [
                        {
                            model: db.Outlet,
                            attributes: [['name', 'outlet']]
                        }
                    ]
                }
            ],
            where: {
                isCustomer: true
            },
            order: ['id']
        }).then((results) => {
            success(res, 'All Users', httpStatus.OK, results);
        });

        // await db.sequelize.query("SELECT users.id, users.email, users.name, users.dateOfBirth, users.phoneNumber, users.gender, users.address, users.orderTotal, users.spentTotal, " +
        //     "users.visitTotal, users.isCustomer, outlets.name as outlet " +

        //     "FROM users " +

        //     "LEFT JOIN outletusers ON users.id = outletusers.OfUser " +
        //     "LEFT JOIN outlets ON outletusers.OfOutlet = outlets.id " +
        //     "WHERE users.isCustomer = true " +

        //     "ORDER BY users.id").spread((results) => {
        //     success(res, 'All Users', httpStatus.OK, results);
        //     // Results will be an empty array and metadata will contain the number of affected rows.
        // })
    } catch (error) {
        return next(error);
    }
};

const createCustomer = async (req, res, next) => {
    try {
        const foundCustomer = await db.User.findOne({where: {email: req.body.email}});
        if (foundCustomer) {
            throw validationErr('Customer', httpStatus.CONFLICT, 'Customer already exists.');
        } else {
            const createdCustomer = await db.User.create(req.body);
            if (createdCustomer) {
                console.log("customer created");

                if (req.body.group.length > 0) {
                    const groupLength = (req.body.group.length - 1);
                    let groupCount = 0;

                    while (groupCount <= groupLength) {
                        console.log("after loop");
                        let assignedGroup = await db.GroupMember.create({
                            OfUser: createdCustomer.id,
                            OfGroup: req.body.group[groupCount].id
                        });
                        console.log("Assigned Group");
                        if (assignedGroup) {
                            //success(res, 'Outlet Assigned.', httpStatus.CREATED, assignedOutlet);
                        } else {
                            throw validationErr('Outlet', httpStatus.CONFLICT, 'Outlet not assigned.');
                        }
                        groupCount++;
                    }
                }


                const outletLength = (req.body.outlet.length - 1);
                let outletCount = 0;
                while (outletCount <= outletLength) {
                    //console.log("inside loop");
                    let assignedOutlet = await db.OutletUser.create({
                        OfUser: createdCustomer.id,
                        OfOutlet: req.body.outlet[outletCount].id
                    });
                    console.log("Assigned outlet");
                    if (assignedOutlet) {
                        //success(res, 'Outlet Assigned.', httpStatus.CREATED, assignedOutlet);
                    } else {
                        throw validationErr('Outlet', httpStatus.CONFLICT, 'Outlet not assigned.');
                    }
                    outletCount++;
                }
                success(res, 'Customer Added.', httpStatus.CREATED, createdCustomer);
            } else {
                throw validationErr('createdCustomer', httpStatus.CONFLICT, 'Customer not created.');
            }
        }
    } catch (error) {
        return next(error);
    }
};

const updateCustomer = async (req, res, next) => {
    try {
        const foundUser = await db.User.findById(req.params.customerId);
        if (!foundUser) {
            throw validationErr('user', httpStatus.CONFLICT, 'User doesn\'t exist.');
        } else {
            const updatedtedUser = await foundUser.update(
                req.body
                , {
                    where: {id: req.params.customerId}
                });
            success(res, 'User Updated.', httpStatus.OK, updatedtedUser);
        }
    } catch (error) {
        return next(error);
    }
};

const deleteCustomer = async (req, res, next) => {
    try {
        const foundUser = await db.User.findById(req.params.customerId);
        if (!foundUser) {
            throw validationErr('user', httpStatus.CONFLICT, 'User doesn\'t exist.');
        } else {
            const deletedUser = await foundUser.destroy({
                where: {id: req.params.customerId}
            });
            success(res, 'User deleted.', httpStatus.OK, deletedUser);
        }
    } catch (error) {
        return next(error);
    }
};


module.exports = {
    createUser,
    login,
    getAllUsers,
    resetPassword,
    updateUser,
    deleteUser,
    createCustomer,
    getAllCustomers,
    updateCustomer,
    deleteCustomer
};