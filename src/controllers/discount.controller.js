var httpStatus = require('http-status');
var validationErr = require('../utils/customValidationError');
var db = require('../config/sequelize');
var {success} = require('../utils/successResponse');


const getAllDiscount = async (req, res, next) => {
    try {
        let discounts = await db.Discount.findAll();
        if (discounts) {
            success(res, 'Following are the Discounts.', httpStatus.CREATED, discounts);
        }
        else
            throw validationErr('Discount', httpStatus.CONFLICT)
    }
    catch (error) {
        return next(error);
    }
};

const addCustomerBasedDiscount = async (req, res, next) => {
    try {
        console.log(req.body);
        let Discount;
        let taskcompleted;
        Discount = await db.Discount.findAll({where: {name: req.body.name}});

        if (Discount.length != 0) {
            throw validationErr('Discount', httpStatus.CONFLICT, 'This Discount already exists.');
        }
        else {
            const createdDiscount = await db.Discount.create(req.body);

            if (req.body.type === "Customer Based") {
                {
                    if (createdDiscount) {
                        let randomGroup = "none";
                        if (req.body.CustomersandGroups.length !== 0) {
                            let i = req.body.CustomersandGroups.length;
                            let j = 0;
                            while (j <= i - 1) {
                                for (let key in req.body.CustomersandGroups[j]) {
                                    if (key == 'groupId') {
                                        findgroup = await db.Group.findById(req.body.CustomersandGroups[j][key]);

                                        if (findgroup.length != 0) {

                                            if (req.body.Foroutlet.length != 0) {
                                                let inn = req.body.Foroutlet.length;
                                                let itr = 0;

                                                while (itr <= inn - 1) {

                                                    for (let key in req.body.Foroutlet[itr]) {
                                                        const assignDiscount = await db.UniversalDiscount.create(
                                                            {
                                                                OfDiscount: createdDiscount.id,
                                                                OfGroup: findgroup.id,
                                                                OfOutlet: req.body.Foroutlet[itr][key]
                                                            });
                                                        if (assignDiscount) {
                                                            taskcompleted = assignDiscount;
                                                        }
                                                        else {
                                                            console.log('Error in assigning discount to groups to  each outlet');
                                                        }

                                                    }
                                                    ++itr;
                                                }


                                            }
                                        }
                                        else {
                                            //throw validationErr('Group', httpStatus.Ok, 'Group not found . Create one');
                                        }
                                    }
                                }
                                ++j;
                            }
                            j = 0;
                            while (j <= i - 1) {

                                for (let key in req.body.CustomersandGroups[j]) {
                                    if (key == 'userId') {
                                        let groupobj;
                                        console.log("here");
                                        let Gname = "Random" + Math.random();
                                        let Gtype = 'Custom';
                                        if (randomGroup == 'none') {
                                            console.log("here i am");
                                            var createdRandom = await db.Group.create({name: Gname, groupType: Gtype});
                                            groupobj = createdRandom;
                                            assignGroupmembers = await db.GroupMember.create({
                                                OfGroup: createdRandom.id,
                                                OfUser: req.body.CustomersandGroups[j][key]
                                            });
                                            randomGroup = 'created';

                                        }
                                        else {
                                            // console.log(createdRandom);
                                            assignGroupmembers = await db.GroupMember.create({
                                                OfGroup: createdRandom.id,
                                                OfUser: req.body.CustomersandGroups[j][key]
                                            });

                                        }


                                    }
                                }
                                ++j;
                            }

                            if (randomGroup == 'created') {
                                if (req.body.Foroutlet.length != 0) {
                                    let inn = req.body.Foroutlet.length;
                                    let itr = 0;
                                    // console.log('here 2');
                                    while (itr <= inn - 1) {
                                        //console.log('here 2'+itr);
                                        for (let key in req.body.Foroutlet[itr]) {
                                            //  console.log(req.body.Foroutlet[itr][key]);


                                            const assignDiscount = await db.UniversalDiscount.create({
                                                OfDiscount: createdDiscount.id,
                                                OfGroup: createdRandom.id,
                                                OfOutlet: req.body.Foroutlet[itr][key]
                                            });
                                            //console.log(assignDiscount);
                                            if (assignDiscount) {
                                                taskcompleted = assignDiscount;
                                            }
                                            else {
                                                console.log('Error in assigning discount to groups to  each outlet');
                                            }

                                        }
                                        ++itr;
                                    }
                                }
                            }
                        }
                    }
                }
                success(res, 'Task Completed', httpStatus.CREATED, taskcompleted);
            }

            else if (req.body.type === "Product Based") {
                if (req.body.Foroutlet.length !== 0) {
                    console.log('has outlet'+req.body.Foroutlet.length)
                    let counter = 0;
                    let limit = req.body.Foroutlet.length;

                    while (counter < limit) {
                        console.log('has products ');
                        for (let key in req.body.Foroutlet[counter]) {
                            if (req.body.Products.length !== 0) {
                                console.log('has products ');
                                let productcounter = 0;
                                let productlimit = req.body.Products.length;
                                while (productcounter < productlimit) {
                                    console.log('here last');
                                    const assignDiscount = await db.UniversalDiscount.create(
                                        {
                                            OfDiscount: createdDiscount.id,
                                            OfVendorProduct: req.body.Products[productcounter].ProductId,
                                            OfOutlet: req.body.Foroutlet[counter][key]
                                        });

                                    ++productcounter;
                                }

                            }


                        }
                        counter++;
                    }
                }


            }

        }
        success(res, 'discount Created .', httpStatus.OK);


    }
    catch (error) {
        return next(error);
    }
};

const updateDiscount = async (req, res, next) => {

    try {

        let Discount;

        Discount = await db.Discount.findAll({where: {name: req.params.discountName}});
        console.log(Discount.length);
        if (Discount.length != 0) {
            const UpdatedDiscount = await db.Discount.update(
                req.body,
                {where: {name: req.params.discountName}}
            );
            success(res, ' Discount Updated.', httpStatus.CREATED, UpdatedDiscount);
        }
        else {
            throw validationErr('email', httpStatus.CONFLICT, 'This name already exist.');

        }

    }
    catch (error) {
        return next(error);
    }

};


const deleteDiscount = async (req, res, next) => {

    try {

        let Dicount;

        Dicount = await db.Discount.findAll({where: {name: req.params.discountName}});
        console.log(Dicount.length);
        if (Dicount.length != 0) {
            const deletedDiscount = await db.Discount.destroy(
                {where: {name: req.params.discountName}}
            );
            success(res, ' Discount Deleted.', httpStatus.CREATED, deletedDiscount);

        }
        else {
            throw validationErr('email', httpStatus.CONFLICT, 'No such Discount.');

        }

    }
    catch (error) {
        return next(error);
    }

};

module.exports = {getAllDiscount, addCustomerBasedDiscount, updateDiscount, deleteDiscount};