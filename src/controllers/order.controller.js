const httpStatus = require('http-status');
const validationErr = require('../utils/customValidationError');
const db = require('../config/sequelize');
const {success} = require('../utils/successResponse');


const getAllOrders = async (req, res, next) => {
    try {
        db.Order.findAll({
            include: [
                {
                    model: db.Vendor,
                    as: 'Vendors',
                    attributes: [['name', 'vendorname']]
                },
                {
                    model: db.User,
                    as: 'Users',
                    attributes: [['name', 'manager']]
                }
            ]
        }).then((results, metadata) => {
                success(res, ' all of products .', httpStatus.OK, results);
            });

        // db.sequelize.query("SELECT vendors.name as vendorname,orders.*,users.name as manager \n" +
        //     "from orders\n" +
        //     "left join vendors on vendors.id=orders.OfVendor\n" +
        //     "LEFT JOIN users on users.id=orders.OfUser").spread((results, metadata) => {
        //     success(res, ' all of products .', httpStatus.OK, results);
        // });
    }
    catch (error) {
        return next(error);
    }


};

const getOrderDetail = async (req, res, next) => {
    try {
        db.Order.findAll({
            include: [
                {
                    model: db.Vendor,
                    as: 'Vendors',
                    attributes: [['name', 'vendorName']]
                },
                {
                    model: db.OrderDetail,
                    as: 'OrderDetails',
                    attributes: [['id', 'detailId'], ['price', 'OrderProductPrice'], ['quantity', 'quantityOrdered'], ['receivedQuantity', 'productrecievedQuantity'], ['grossAmount', 'productgrossAmount'], ['OfProduct', 'productId'],['amount', 'ProductAmount']],
                    include: [
                        {
                            model: db.Product,
                            as: 'Products',
                            attributes: [['name', 'ProductName'], ['inHandQuantity', 'ProductinHandQuantity'], ['minimumQuantity', 'reOrderPoint']]
                        }
                    ]
                },
                {
                    model: db.User,
                    as: 'Users',
                    attributes: [['name', 'manager']]
                }
            ],
            where: {
                id: req.params.orderId
            }
        }).then((results, metadata) => {
            success(res, ' all of products .', httpStatus.OK, results);
        });

        // db.sequelize.query("SELECT vendors.name as vendorName,orders.*,orderdetails.id as detailId,orderdetails.price as OrderProductPrice," +
        //     "orderdetails.quantity as quantityOrdered,orderdetails.receivedQuantity as productrecievedQuantity," +
        //     "orderdetails.grossAmount as productgrossAmount,orderdetails.OfProduct as productId," +
        //     "orderdetails.amount as ProductAmount,products.name as ProductName," +
        //     "products.inHandQuantity as ProductinHandQuantity,products.minimumQuantity as reOrderPoint,users.name as manager \n" +
        //     "from orders\n" +
        //     "left join vendors on vendors.id=orders.OfVendor\n" +
        //     "left join orderdetails on orders.id=orderdetails.OfOrder\n" +
        //     "left join products on orderdetails.OfProduct=products.id\n" +
        //     "LEFT JOIN users on users.id=orders.OfUser where orders.id='"+req.params.orderId +"'").spread((results, metadata) => {
        //     success(res, ' all of products .', httpStatus.OK, results);
        // });
    }
    catch (error) {
        return next(error);
    }


};

const getAllOrdersOfOutlet = async (req, res, next) => {
    try {
        console.log('here');
        db.Order.findAll({
            include: [
                {
                    model: db.Vendor,
                    as: 'Vendors',
                    attributes: [['name', 'vendorname']]
                },
                {
                    model: db.User,
                    as: 'Users',
                    attributes: [['name', 'manager']]
                }
            ],
            where: {
                OfOutlet: req.params.outletId
            }
        }).then((results, metadata) => {
            success(res, ' all of products .', httpStatus.OK, results);
        });

        // db.sequelize.query("SELECT vendors.name as vendorname,orders.*,users.name as manager \n" +
        //     "from orders\n" +
        //     "left join vendors on vendors.id=orders.OfVendor\n" +
        //     "LEFT JOIN users on users.id=orders.OfUser\n"+
        //     "where orders.OfOutlet = '"+req.params.outletId +"'").spread((results, metadata) => {
        //     success(res, ' all of products .', httpStatus.OK, results);
        // });
    }
    catch (error) {
        return next(error);
    }


};


const createOrder = async (req, res, next) => {

    try {
        const foundSupplier = await db.Vendor.findById(req.body.OfVendor);

        if (foundSupplier.length != 0)   // if suppier exists
        {
            const createdOrder = await db.Order.create({
                name: req.body.name, amount: req.body.amount,
                grossAmount: req.body.grossAmount, expectedDate: req.body.expectedDate, OfVendor: req.body.OfVendor,
                status: req.body.status, dateOfOrder: req.body.dateOfOrder, receivedDate: req.body.receivedDate,
                OfVendor: req.body.OfVendor, OfOutlet: req.body.outletId, OfUser: req.body.OfUser
            });


            if (createdOrder.length != 0)        //if order is created
            {
                if (req.body.orderedItems.length != 0)  //creating order details
                {
                    for (let i = 0; i < req.body.orderedItems.length; i++) {
                        const createdOrderDetail = await db.OrderDetail.create({
                            price: req.body.orderedItems[i].price,
                            quantity: req.body.orderedItems[i].quantity,
                            receivedQuantity: req.body.orderedItems[i].receivedQuantity,
                            amount: req.body.orderedItems[i].amount,
                            discount: req.body.orderedItems[i].discount,
                            OfOrder: createdOrder.id,
                            grossAmount: req.body.orderedItems[i].grossAmount,
                            note: req.body.orderedItems[i].note,
                            OfProduct: req.body.orderedItems[i].OfProduct
                        });
                        if (createdOrderDetail.length != 0) {
                            console.log(createdOrderDetail.id + "  is created");
                        }
                    }
                }


                success(res, 'New Order Created.', httpStatus.CREATED, createdOrder);
            }


        }
        else
            throw validationErr('Brand', httpStatus.CONFLICT, 'Brand doesn\'t exist.');


    }
    catch (error) {
    }

};

const updateOder = async (req, res, next) => {
    try {

       let detailList = [];
        const foundOrder = await db.Order.findById(req.params.orderId);

        if (foundOrder.length != 0)   // if suppier exists
        {
            const updatedOrder = await db.Order.update({
                name: req.body.name, amount: req.body.amount,
                grossAmount: req.body.grossAmount, expectedDate: req.body.expectedDate, OfVendor: req.body.OfVendor,
                status: req.body.status, dateOfOrder: req.body.dateOfOrder, receivedDate: req.body.receivedDate,
                OfVendor: req.body.OfVendor, OfOutlet: req.body.outletId, OfUser: req.body.OfUser
                }
                ,
            {
                where: { id: req.params.orderId }
            }
            );




            if (updatedOrder.length != 0)
            {

                if (req.body.orderedItems.length != 0)
                {
                    console.log('array lenght is    '+req.body.orderedItems.length );
                    const detailList = await db.OrderDetail.findAll({
                        attributes: ['OfProduct'],
                        where: {
                            OfOrder: req.params.orderId
                        }
                    });

                    let a ={};
                    if(detailList.length != 0){
                        console.log(detailList.length + "length of array is" );
                        for ( let i = 0; i < detailList.length; i++) {

                            console.log(i + "       is" + detailList[i].OfProduct );

                          if (a= req.body.orderedItems.find(item =>item.OfProduct == detailList[i].OfProduct )) {
                                console.log('updating in database  for    '+ a);
                              const updateOrderDetail = await db.OrderDetail.update({
                                  price: a.price,
                                  quantity: a.quantity,
                                  receivedQuantity: a.receivedQuantity,
                                  amount: a.amount,
                                  discount: a.discount,
                                  OfOrder: a.orderId,
                                  grossAmount: a.grossAmount,
                                  note: a.note,
                                  OfProduct: a.OfProduct
                              },
                                  {
                                      where: { OfProduct:  detailList[i].OfProduct ,
                                          OfOrder: req.params.orderId }
                                  }
                              );
                              if(updateOrderDetail.length!=0)
                              {
                                  console.log('updated');
                                  req.body.orderedItems.splice(req.body.orderedItems.findIndex(item => item.OfProduct ==a.OfProduct),1);
                                  console.log('array lenght after splice  is    '+req.body.orderedItems.length );
                                  console.log('array lenght of detailList after splice  is    '+detailList.length );
                              }

                            }
                            else
                          {
                              console.log('also here'+ detailList[i].OfProduct);
                              const deletedDetail = await db.OrderDetail.destroy({

                                  where: {OfProduct: detailList[i].OfProduct,
                                      OfOrder: req.params.orderId}
                              });

                          }



                        }

                        if(req.body.orderedItems.length != 0){
                            console.log('here i  a m');
                            let j=0;
                            while(j < req.body.orderedItems.length) {
                                console.log('creating for    ' + req.body.orderedItems[j].OfProduct);
                                const createdOrderDetail = await db.OrderDetail.create({
                                    price: req.body.orderedItems[j].price,
                                    quantity: req.body.orderedItems[j].quantity,
                                    receivedQuantity: req.body.orderedItems[j].receivedQuantity,
                                    amount: req.body.orderedItems[j].amount,
                                    discount: req.body.orderedItems[j].discount,
                                    OfOrder:  req.params.orderId,
                                    grossAmount: req.body.orderedItems[j].grossAmount,
                                    note: req.body.orderedItems[j].note,
                                    OfProduct: req.body.orderedItems[j].OfProduct
                                });
                                j++;


                            }

                        }


                    }



                }


                success(res, 'New Order Created.', httpStatus.CREATED, updatedOrder);
            }


        }
        else
            throw validationErr('Brand', httpStatus.CONFLICT, 'Brand doesn\'t exist.');


    }
    catch (error) {
    }
};

module.exports = {getAllOrders, createOrder, updateOder,getOrderDetail,getAllOrdersOfOutlet};