const httpStatus = require('http-status');
const validationErr = require('../utils/customValidationError');
const db = require('../config/sequelize');
const {success} = require('../utils/successResponse');

// CRUD Role Start

const getRoles = async (req, res, next) => {
    try {
        let roles = await db.Role.findAll();
        success(res, 'All User Roles', httpStatus.OK, roles);
    } catch (error) {
        return next(error);
    }
};

const createRole = async (req, res, next) => {
    try {
        console.log(req.body.name);
        let newUserRoles = await db.Role.findAll({where: {name: req.body.roleName}});
        if (newUserRoles.length !== 0) {
            throw validationErr('role', httpStatus.CONFLICT, 'This User role already exists.');
        }
        else {
            console.log('here');
            const createdUserRoles = await db.Role.create({
                name: req.body.name,
                description: req.body.description
            });
            if (createdUserRoles) {
                console.log('created Role: ' + JSON.stringify(createdUserRoles) + '\nID: ' + createdUserRoles.id);
                //success(res, 'New Role Added.', httpStatus.CREATED, createdUserRoles);
                if (req.body.permissions.length !== 0) {
                    let i = req.body.permissions.length ;
                    let j = 0;
                    while (j < i) {
                        const foundPermission = await db.Permission.findAll({
                            where: {
                                type: req.body.permissions[j].type,
                                canEdit: req.body.permissions[j].canEdit,
                                canView: req.body.permissions[j].canView
                            }
                        });
                        console.log('found Permission: ' + JSON.stringify(foundPermission) + '\nID: ' + foundPermission[0].id);
                        if (foundPermission.length !== 0) {
                            //success(res, 'issNew Permission Added.', httpStatus.CREATED, createdPermission);
                            const AssignedPermission = await db.RolePermission.create({
                                OfPermission: foundPermission[0].id,
                                OfRole: createdUserRoles.id
                            });
                            if (AssignedPermission) {
                                console.log('Permission assigned');
                            } else {
                                console.log('Permission not assigned');
                            }
                        } else {
                            console.log('Permission not created');
                        }
                        ++j;
                    }
                    success(res, 'New Role Added.', httpStatus.CREATED, createdUserRoles);
                } else {
                    success(res, 'New Role Added.', httpStatus.CREATED, createdUserRoles);
                }
            }
        }
    }
    catch (error) {
        return next(error);
    }
};

const updateRole = async (req, res, next) => {
    try {
        const foundRole = await db.UserRole.findById(req.params.roleId);
        if (!foundRole) {
            throw validationErr('user role', httpStatus.CONFLICT, 'User Role doesn\'t exist.');
        } else {
            const updatedRole = await foundRole.update(
                req.body
                , {
                    where: {id: req.params.roleId}
                });
            success(res, 'Role Updated.', httpStatus.OK, updatedRole);
        }
    } catch (error) {
        return next(error);
    }
};
const deleteRole = async (req, res, next) => {
    try {
        const foundRole = await db.Role.findById(req.params.roleId);
        if (!foundRole) {
            throw validationErr('role', httpStatus.CONFLICT, 'Role doesn\'t exist.');
        } else {
            const deletedRole = await foundRole.destroy({
                where: {id: req.params.roleId}
            });
            success(res, 'Order deleted.', httpStatus.OK, deletedRole);
        }
    } catch (error) {
        return next(error);
    }
};

// CRUD Role End

// CRUD Permission Start
const getPermission = async (req, res, next) => {
    try {
        await db.Permission.findAll({
            attributes: ['type', 'id'],
            group: ['type', 'id'],
        }).then((results, metadata) => {
            success(res, 'All Users', httpStatus.OK, results);
        });

        // await db.sequelize.query('SELECT DISTINCT (permissions.type), permissions.id FROM permissions').spread((results, metadata) => {
        //     success(res, 'All Users', httpStatus.OK, results);
        //     // Results will be an empty array and metadata will contain the number of affected rows.
        // });
        
    } catch (error) {
        return next(error);
    }
};
const createPermission = async (req, res, next) => {
    try {
        let createdPermission = db.Permission.bulkCreate(req.body.permission);
        if (createdPermission.length !== 0) {
            success(res, 'All User Roles', httpStatus.OK, createdPermission);
        } else {
            success(res, 'Error', httpStatus.BADNAME, 'Permission not created');
        }
    } catch (e) {

    }
};

module.exports = {createRole, getRoles, updateRole, deleteRole, getPermission, createPermission};