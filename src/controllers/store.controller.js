const httpStatus = require('http-status');
const validationErr = require('../utils/customValidationError');
const db = require('../config/sequelize');
const { success } = require('../utils/successResponse');

// CRUD Start

const getStores = async (req, res, next) => {
    try {
        let stores = await db.Store.findAll();
        // console.log('Users: \n' + JSON.stringify(users));
        success(res, 'ALL_STORES', httpStatus.OK, stores);
    } catch (error) {
        return next(error);
    }
};

const createStore = async(req, res, next) => {
    try {
            const createdStore = await db.Store.create(req.body);
            success(res, 'New Store Created.', httpStatus.CREATED, createdStore);
    } catch (error) {
        return next(error);
    }
};

const updateStore = async(req, res, next) => {
    try {
        const foundStore = await db.Store.findById(req.params.storeId);
        if (!foundStore) {
            throw validationErr('store', httpStatus.CONFLICT, 'Store doesn\'t exist.');
        } else {
            const updatedtedStore = await foundStore.update(
                req.body
                ,{
                    where: { id: req.params.storeId }
                });
            success(res, 'Store Updated.', httpStatus.OK, updatedtedStore);
        }
    } catch (error) {
        return next(error);
    }
};
const deleteStore = async(req, res, next) => {
    try {
        const foundStore = await db.Store.findById(req.params.storeId);
        if (!foundStore) {
            throw validationErr('store', httpStatus.CONFLICT, 'Store doesn\'t exist.');
        } else {
            const deletedStore = await foundStore.destroy({
                where: { id: req.params.storeId }
            });
            success(res, 'Store deleted.', httpStatus.OK, deletedStore);
        }
    } catch (error) {
        return next(error);
    }
};

// CRUD End

module.exports =  { createStore, getStores, updateStore, deleteStore };