const httpStatus = require('http-status');
const validationErr = require('../utils/customValidationError');
const db = require('../config/sequelize');
const { success } = require('../utils/successResponse');

// CRUD Start

const getVendors = async (req, res, next) => {
    try {
        await db.Vendor.findAll({
            include: [
                {
                    model: db.VendorProduct,
                    as: 'VendorProducts',
                    include: [
                        {
                            model: db.Product,
                            as: 'Products',
                            attributes: [[db.sequelize.fn('COUNT', 'id'), 'total products']]
                        }
                    ]
                }
            ],
            group: ['id'],
            order: ['name']
        }).then((results) => {
            success(res, 'All Vendors', httpStatus.OK, results);
        });

        // await db.sequelize.query("SELECT vendors.*, count(products.id) as `total products` " +
        //     "FROM vendors " +
        //     "LEFT JOIN  vendorproducts ON vendors.id = vendorproducts.OfVendor " +
        //     "LEFT JOIN products ON vendorproducts.OfProduct =products.id " +
        //     "GROUP BY vendors.id " +
        //     "ORDER BY (vendors.name) ASC").spread((results) => {
        //     success(res, 'All Vendors', httpStatus.OK, results);
        // });

        // console.log('Users: \n' + JSON.stringify(users));
    } catch (error) {
        return next(error);
    }
};

const getVendorsofOutlets = async (req, res, next) => {
    try {
        console.log("here");
        await db.Vendor.findAll({
            attributes: [['name', 'vendorname'], ['id', 'vendorid']],
            include: [
                {
                    model: db.VendorProduct,
                    as: 'VendorProducts',
                    attributes: ['supplierPrice'],
                    include: [
                        {
                            model: db.Product,
                            as: 'Products',
                            attributes: []
                        }
                    ]
                }
            ],
            where: {
                OfOutlet: req.params.outletId
            },
            group: ['vendorname']
        }).then((results) => {
            success(res, 'All outlets', httpStatus.OK, results);
        });

        //     await db.sequelize.query(" SELECT  vendors.name as vendorname ,vendors.id as \"vendorid\" ,vendorproducts.supplierPrice\n" +
        //     "                       FROM vendors \n" +
        //     "                       LEFT JOIN vendorproducts  ON vendors.id=vendorproducts.OfVendor\n" +
        //     "                        LEFT JOIN products ON vendorproducts.OfProduct=products.id\n" +
        //     "                        WHERE vendors.OfOutlet=" +req.params.outletId+"\n" +
        //     "                        GROUP BY vendorname\n" +
        //     "                        ").spread((results) => {
        //     success(res, 'All outlets', httpStatus.OK, results);
        // });
        // console.log('Users: \n' + JSON.stringify(users));
    } catch (error) {
        return next(error);
    }
};
const createVendor = async(req, res, next) => {
    try {
        let foundVendor = await db.Vendor.findAll({where: {name: req.body.name}});
        if (foundVendor.length!==0) {
            throw validationErr('vendor', httpStatus.CONFLICT, 'Vendor already exists.');
        }
        else{
            const createdVednor = await db.Vendor.create(req.body);
            success(res, 'New Vendor Created.', httpStatus.CREATED, createdVednor);
        }
    } catch (error) {
        return next(error);
    }
};
const getSpecificSupplierOfOutlet = async (req, res, next) => {
    try {
        // console.log("here");
        await db.Product.findAll({
            group: [db.sequelize.literal('`VendorProducts.Vendors.vendorname`')],
            attributes: [],
            include: [
                {
                    model: db.VendorProduct,
                    as: 'VendorProducts',
                    attributes: ['supplierPrice'],
                    include: [
                        {
                            model: db.Vendor,
                            as: 'Vendors',
                            attributes: [['id', 'vendorId'], ['name', 'vendorname']]
                        }
                    ]
                },
                {
                    model: db.Brand,
                    attributes: []
                },
                {
                    model: db.Category,
                    attributes: []
                }
            ],
            where: {
                OfCategory: req.params.categoryId,
                OfBrand: req.params.brandId
            }
        }).then((results) => {
            success(res, 'All Vendors', httpStatus.OK, results);
        });

        // await db.sequelize.query(" SELECT  vendors.id as \"vendorId\",vendors.name as " +
        //     "                       vendorname,vendorproducts.supplierPrice\n" +
        //     "                       FROM products \n" +
        //     "                       LEFT JOIN vendorproducts  ON products.id=vendorproducts.OfProduct\n" +
        //     "                        LEFT JOIN vendors ON vendorproducts.OfVendor=vendors.id\n" +
        //     "                        LEFT JOIN brands on products.OfBrand=brands.id\n" +
        //     "                        LEFT JOIN categories on products.OfCategory=categories.id\n" +
        //     "                        WHERE products.OfCategory="+req.params.categoryId+"\n" +
        //     "                        AND products.OfBrand="+req.params.brandId+"\n"+
        //     "                        GROUP BY vendorname\n" ).spread((results) => {

        //     success(res, 'All Vendors', httpStatus.OK, results);
        // });

    } catch (error) {
        return next(error);
    }
};

const updateVendor = async(req, res, next) => {
    try {
        const foundVendor = await db.Vendor.findById(req.params.vendorId);
        if (!foundVendor) {
            throw validationErr('vendor', httpStatus.CONFLICT, 'Vendor doesn\'t exist.');
        } else {
            const updatedVendor = await foundVendor.update(
                req.body
                ,{
                    where: { id: req.params.vendorId }
                });
            success(res, 'Vendor Updated.', httpStatus.OK, updatedVendor);
        }
    } catch (error) {
        return next(error);
    }
};
const deleteVendor = async(req, res, next) => {
    try {
        console.log('again in delete')
        const foundVendor = await db.Vendor.findById(req.params.vendorId);
        let deletedVendor;
        if (!foundVendor) {
            throw new validationErr('vendor', httpStatus.CONFLICT, 'Vendor doesn\'t exist.');
        } else {
             deletedVendor = await db.Vendor.destroy({
                where: { id: req.params.vendorId }
            });

            success(res, 'Vendor deleted.', httpStatus.OK, deletedVendor);

        }
    } catch (error) {
        console.log("Hello eerror",error);
        return next(error);
    }
};

// CRUD End

module.exports =  { createVendor, getVendors, updateVendor, deleteVendor, getSpecificSupplierOfOutlet,getVendorsofOutlets };