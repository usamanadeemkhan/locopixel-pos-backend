var httpStatus = require('http-status');
var validationErr = require('../utils/customValidationError');
var db = require('../config/sequelize');
const {success} = require('../utils/successResponse');

const getStockProducts = async (req, res, next) => {
    try {
        let products;
        let getTags;
        db.Product.findAll({
            attributes: ['id', 'name', 'inHandQuantity', 'minimumQuantity', 'createdAt'],
            include: [
                {
                    model: db.VendorProduct,
                    as: 'VendorProducts',
                    attributes: ['id'],
                    include: [
                        {
                            model: db.Vendor,
                            attributes: [['id', 'vendorId'], ['name', 'vendorname']]

                        }
                    ]
                }
            ],
            where: {
                OfOutlet: req.params.outletId
            }
        }).then((results, metadata) => {
            success(res, ' all products .', httpStatus.OK, results);
        });

        // db.sequelize.query("SELECT products.id,products.name,"+
        //     "products.inHandQuantity,products.minimumQuantity," +
        //     "products.createdAt,vendors.id as \"vendor id\",vendors.name as \"vendorname\"" +

        //     "FROM products\n" +
        //     "LEFT JOIN vendorproducts vp ON products.id=vp.OfProduct\n" +
        //     "LEFT JOIN vendors ON vp.OfVendor=vendors.id\n" +
        //     "WHERE products.OfOutlet=" + req.params.outletId + "").spread((results, metadata) => {
        //     success(res, ' all products .', httpStatus.OK, results);
        // });


    }
    catch (error) {
        return next(error);
    }


};

const updateProduct = async (req, res, next) => {

    try {

        let updtdProduct;
        updtdProduct = await db.Outlet.findAll({where: {id: req.params.productId}});

        if (updtdProduct.length != 0) {
            const UpdatedProduct = await db.Product.update(
                req.body,
                {where: {id: req.params.productId}}
            );
            success(res, ' Product Updated.', httpStatus.CREATED, UpdatedProduct);
        }
        else {
            throw validationErr('email', httpStatus.CONFLICT, 'This name already exist.');

        }

    }
    catch (error) {
    }

};







module.exports = {
    getStockProducts,
    updateProduct,
};