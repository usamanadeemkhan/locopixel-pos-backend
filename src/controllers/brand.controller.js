const httpStatus = require('http-status');
const validationErr = require('../utils/customValidationError');
const db = require('../config/sequelize');
const {success} = require('../utils/successResponse');

// CRUD Start

const getBrand = async (req, res, next) => {
    try {
        let Brands = await db.Brand.findAll();

        if (Brands.length != 0) {
            success(res, 'All Brands List', httpStatus.OK, Brands);
        }
        else {
            console.log('empty');
        }

    } catch (error) {
        return next(error);
    }
};

const getBrandofOutlet = async (req, res, next) => {
    try {
        await db.Brand.findAll({where: {OfOutlet: req.params.outletId}}).then((results) => {
            success(res, 'All brands of outlet', httpStatus.OK, results);
        });

        // await db.sequelize.query(" SELECT DISTINCT brands.* " +
        //     "FROM brands " +
        //     "WHERE brands.OfOutlet = " + req.params.outletId ).spread((results) => {
        //     success(res, 'All brands of outlet', httpStatus.OK, results);
        // });

    } catch (error) {
        return next(error);
    }
};

const createBrand = async (req, res, next) => {
    try {
        const foundBrand = await db.Brand.findAll({where: {name: req.body.name}});

        if (foundBrand.length == 0) {
            const createdBrand = await db.Brand.create(req.body);
            success(res, 'New Brand Created.', httpStatus.CREATED, createdBrand);

        }
        else
            throw validationErr('Brand', httpStatus.CONFLICT, 'Brand doesn\'t exist.');

    } catch (error) {
        return next('error');
    }
};

const updateBrand = async (req, res, next) => {
    try {
        const foundBrand = await db.Brand.findById(req.params.brandId);
        if (!foundBrand) {
            throw validationErr('Brand', httpStatus.CONFLICT, 'Brand doesn\'t exist.');
        } else {
            const updatedtedBrand = await foundBrand.update(
                req.body
                , {
                    where: {id: req.params.BrandId}
                });
            success(res, 'Brand Updated.', httpStatus.OK, updatedtedBrand);
        }
    } catch (error) {
        return next(error);
    }
};
const deleteBrand = async (req, res, next) => {
    try {
        const foundBrand = await db.Brand.findById(req.params.BrandId);
        if (!foundBrand) {
            throw validationErr('Brand', httpStatus.CONFLICT, 'Brand doesn\'t exist.');
        } else {
            const deletedBrand = await foundBrand.destroy({
                where: {id: req.params.BrandId}
            });
            success(res, 'Brand deleted.', httpStatus.OK, deletedBrand);
        }
    } catch (error) {
        return next(error);
    }
};

// CRUD End

module.exports = {createBrand, getBrand, updateBrand, deleteBrand, getBrandofOutlet};