module.exports = (sequelize, DataTypes) => {
    const GroupMember = sequelize.define('GroupMember', {});

    GroupMember.associate = models => {
        GroupMember.belongsTo(models.Group, {foreignKey: 'OfGroup'});
        GroupMember.belongsTo(models.User, {foreignKey: 'OfUser'});
    };
    return GroupMember;
};