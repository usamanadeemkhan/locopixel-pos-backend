module.exports = (sequelize, DataTypes) => {
    const Permission = sequelize.define('Permission', {
        type: {
            type: DataTypes.ENUM('Sales', 'Customers', 'Reporting', 'Employees', 'Products')
        },
        canEdit: {
            type: DataTypes.BOOLEAN
        },
        canView: {
            type: DataTypes.BOOLEAN
        }
    });
    Permission.associate = models => {
        Permission.hasMany(models.RolePermission, {as: 'RolePermissions', foreignKey: 'OfPermission', onDelete: 'cascade'});
    };
    return Permission;
};