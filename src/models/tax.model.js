module.exports = (sequelize, DataTypes) => {
    const Tax = sequelize.define('Tax', {
        name: {
            type: DataTypes.STRING,
            allowNull: true
        },
        rate: {
            type: DataTypes.DOUBLE,
            allowNull: true
        },
    });
    Tax.associate = models => {
        Tax.hasMany(models.TaxOutlet, { as : "TaxOutlets", foreignKey: "OfTax", onDelete: 'cascade'});
    };

    return Tax;
};