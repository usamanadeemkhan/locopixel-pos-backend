module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('User', {
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        password: {
            type: DataTypes.STRING,
            allowNull: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            allowNull: true
        },
        dateOfBirth: {
            type: DataTypes.DATE,
            allowNull: true
        },
        phoneNumber: {
            type: DataTypes.STRING,
            allowNull: true
        },
        gender: {
            type: DataTypes.ENUM('Male', 'Female'),
            allowNull: true
        },
        address: {
            type: DataTypes.STRING
        },
        country: {
            type: DataTypes.STRING
        },
        city: {
            type: DataTypes.STRING
        },
        image: {
            type: DataTypes.BLOB
        },
        orderTotal: {
            type: DataTypes.INTEGER
        },
        spentTotal: {
            type: DataTypes.DOUBLE
        },
        visitTotal: {
            type: DataTypes.INTEGER
        },
        isAdmin: {
            type: DataTypes.BOOLEAN
        },
        isCustomer: {
            type: DataTypes.BOOLEAN
        },


    });

    User.associate = models => {
        User.hasMany(models.UserRole, { as : 'UserRoles', foreignKey: 'OfUser', onDelete: 'cascade'});
        User.hasMany(models.Store, {as : 'Stores', foreignKey: 'OfUser'});
        User.hasMany(models.GroupMember, {as : 'GroupMembers', foreignKey: 'OfUser', onDelete: 'cascade'});
        User.hasMany(models.Order, {as : 'Orders', foreignKey: 'OfUser'});
        User.hasMany(models.Invoice, {as : 'CustomerInvoices', foreignKey: 'OfCustomer'});
        User.hasMany(models.Invoice, {as : 'UserInvoices', foreignKey: 'OfUser'});
        User.hasMany(models.Product, {as : 'Products', foreignKey: 'OfUser'});
        User.hasMany(models.OutletUser, {as : 'OutletUsers', foreignKey: 'OfUser', onDelete: 'cascade'});

    };
    return User;
};
