module.exports = (sequelize, DataTypes) => {
    const Discount = sequelize.define('Discount', {
        name: DataTypes.STRING,
        description: DataTypes.TEXT,
        type: DataTypes.ENUM("Product Based", "Customer Based"),
        percentage: DataTypes.INTEGER,
        status: DataTypes.ENUM('Active', 'Deactive'),
        startDate: DataTypes.DATE,
        endDate: DataTypes.DATE,
    });
    Discount.associate = models => {
        Discount.hasMany(models.UniversalDiscount, {as: 'UniversalDiscounts', foreignKey: 'OfDiscount',  onDelete: 'cascade' });
    };
    return Discount;
};