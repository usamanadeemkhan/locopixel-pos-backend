module.exports = (sequelize, DataTypes) => {
    const Store = sequelize.define('Store', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        noOfOutlet: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
    });

    Store.associate = models => {
        Store.belongsTo(models.User, {foreignKey: 'OfUser'});
        Store.hasMany(models.Outlet, {as: 'Outlets', foreignKey: 'OfStore', onDelete: 'cascade'});
    };
    return Store;
};
