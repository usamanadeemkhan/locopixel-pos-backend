module.exports = (sequelize, DataTypes) => {
    const UserRole = sequelize.define('UserRole', {});
    UserRole.associate = models => {
        UserRole.belongsTo(models.User, { foreignKey: 'OfUser'});
        UserRole.belongsTo(models.Role, { foreignKey: 'OfRole'});
    };
    return UserRole;
};