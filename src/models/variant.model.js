module.exports = (sequelize, DataTypes) => {
    const Variant = sequelize.define('Variant', {
        name: DataTypes.STRING,

        sku: DataTypes.INTEGER,
    });
    Variant.associate = models => {

        Variant.hasMany(models.VendorProduct, {as: 'VendorProducts', foreignKey: 'OfVarient'});
    };
    return Variant;
};