module.exports = (sequelize, DataTypes) => {
    const InvoiceDetail= sequelize.define('InvoiceDetail', {
        quantity: {
            type: DataTypes.INTEGER,

        },
        price: {
            type: DataTypes.INTEGER,

        },
        amount: {
            type: DataTypes.INTEGER,

        },
        discount: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        grossAmount: {
            type: DataTypes.INTEGER,
            allowNull: true
        },

    });

    InvoiceDetail .associate = models => {
        InvoiceDetail.belongsTo(models.Invoice, {foreignKey: 'OfInvoice'});
        InvoiceDetail.belongsTo(models.Product, {as:'Products', foreignKey: 'OfProduct'});
    };
    return InvoiceDetail;
};