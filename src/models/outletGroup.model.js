module.exports = (sequelize, DataTypes) => {
    const OutletGroup = sequelize.define('OutletGroup', {

        // completedBatches: { type: DataTypes.INTEGER }
    });

    OutletGroup.associate = models => {
        OutletGroup.belongsTo(models.Outlet, {foreignKey: 'OfOutlet'});
        OutletGroup.belongsTo(models.Group, {foreignKey: 'OfGroup'});

    };
    return OutletGroup;
};