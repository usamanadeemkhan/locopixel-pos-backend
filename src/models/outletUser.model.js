module.exports = (sequelize, DataTypes) => {
    const OutletUser = sequelize.define('OutletUser', {

        // completedBatches: { type: DataTypes.INTEGER }
    });

    OutletUser.associate = models => {
        OutletUser.belongsTo(models.Outlet, {foreignKey: 'OfOutlet'});
        OutletUser.belongsTo(models.User, {foreignKey: 'OfUser'});

    };
    return OutletUser;
};