module.exports = (sequelize, DataTypes) => {
    const OrderDetail = sequelize.define('OrderDetail', {
        price: {
            type: DataTypes.DOUBLE,
            allowNull: true
        },
        quantity: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        receivedQuantity: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        amount: {
            type: DataTypes.DOUBLE,
            allowNull: true
        },
        discount: {
            type: DataTypes.DOUBLE,
            allowNull: true
        },
        grossAmount: {
            type: DataTypes.DOUBLE,
            allowNull: true
        },
        note: {
            type: DataTypes.TEXT,
            allowNull: true
        },
    });

    OrderDetail .associate = models => {
        OrderDetail.belongsTo(models.Order, {foreignKey: 'OfOrder'});
        OrderDetail.belongsTo(models.Product, {as: 'Products', foreignKey: 'OfProduct'});
    };
    return OrderDetail;
};