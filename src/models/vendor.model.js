module.exports = (sequelize, DataTypes) => {
    const Vendor = sequelize.define('Vendor', {
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        phoneNumber: {
            type: DataTypes.STRING,
            allowNull: true
        },
        address: {
            type: DataTypes.STRING
        },
        country: {
            type: DataTypes.STRING
        },
        city: {
            type: DataTypes.STRING
        },
        status: {
            type: DataTypes.BOOLEAN
        },
    });

    Vendor.associate = models => {
        Vendor.belongsTo(models.Outlet, {foreignKey: 'OfOutlet'});
        Vendor.hasMany(models.VendorProduct, {as: 'VendorProducts', foreignKey: 'OfVendor'});
        Vendor.hasMany(models.Order, {as: 'Orders', foreignKey: 'OfVendor'});
    };
    return Vendor;
};
