module.exports = (sequelize, DataTypes) => {
    const RolePermission = sequelize.define('RolePermission', { });
    RolePermission.associate = models => {
        RolePermission.belongsTo(models.Permission, { foreignKey: 'OfPermission'});
        RolePermission.belongsTo(models.Role, { foreignKey: 'OfRole'});
    };
    return RolePermission;
};