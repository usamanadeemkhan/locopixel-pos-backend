module.exports = (sequelize, DataTypes) => {
    const UniversalDiscount = sequelize.define('UniversalDiscount', {
         });

    UniversalDiscount.associate = models => {
        UniversalDiscount.belongsTo(models.Outlet, { foreignKey: "OfOutlet"});
        UniversalDiscount.belongsTo(models.Group, { foreignKey: "OfGroup"});
        UniversalDiscount.belongsTo(models.Product, { foreignKey: "OfProduct"});
        UniversalDiscount.belongsTo(models.Discount, { foreignKey: "OfDiscount"} );
        UniversalDiscount.belongsTo(models.VendorProduct, {as:'VendorProducts', foreignKey: "OfVendorProduct"} );
    };

    return UniversalDiscount;
};