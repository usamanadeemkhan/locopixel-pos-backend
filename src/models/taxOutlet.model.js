module.exports = (sequelize, DataTypes) => {
    const TaxOutlet = sequelize.define('TaxOutlet', {});
    TaxOutlet.associate = models => {
        TaxOutlet.belongsTo(models.Tax, { foreignKey: "OfTax"});
        TaxOutlet.belongsTo(models.Outlet, { foreignKey: "OfOutlet"});
    };

    return TaxOutlet;
};