module.exports = (sequelize, DataTypes) => {
    const Group = sequelize.define('Group', {
        name: DataTypes.STRING,
        description: DataTypes.STRING,
        groupType: DataTypes.ENUM('Custom', 'Default'),
    });

    Group.associate = models => {
        Group.hasMany(models.OutletGroup, { as: 'OutletGroups',foreignKey: 'OfGroup', onDelete: 'cascade'});
        Group.hasMany(models.GroupMember, {as: 'GroupMembers', foreignKey: 'OfGroup', onDelete: 'cascade'});
        Group.hasMany(models.UniversalDiscount, {as: 'UniversalDiscounts', foreignKey: 'OfGroup', onDelete: 'cascade'});

    };
    return Group;
};