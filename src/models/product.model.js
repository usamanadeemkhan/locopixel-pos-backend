module.exports = (sequelize, DataTypes) => {
    const Product = sequelize.define('Product', {
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        description: {
            type: DataTypes.STRING,
            allowNull: false
        },
        supplyPrice: {
            type: DataTypes.DOUBLE,
            allowNull: true
        },
        retailPrice: {
            type: DataTypes.DOUBLE,
            allowNull: true
        },
        markup: {
            type: DataTypes.DOUBLE,
            allowNull: true
        },
        totalPrice: {
            type: DataTypes.DOUBLE,
            allowNull: true
        },
        productType: {
            type: DataTypes.ENUM('Single', 'Variant'),
            allowNull: true
        },
        barcode: {
            type: DataTypes.STRING,
            unique:true
        },
        availability:{
            type: DataTypes.BOOLEAN
        },
        image: {
            type: DataTypes.BLOB
        },
        sku: {
            type: DataTypes.INTEGER
        },
        batchNumber: {
            type: DataTypes.INTEGER
        },
        inHandQuantity: {
            type: DataTypes.INTEGER
        },
        minimumQuantity: {
            type: DataTypes.INTEGER
        },
        expiryDate: {
            type: DataTypes.DATE
        },
    });

    Product.associate = models => {
        Product.belongsTo(models.User, {foreignKey: 'OfUser'});
        Product.belongsTo(models.Outlet, {foreignKey: 'OfOutlet'});
        Product.belongsTo(models.Category, {foreignKey: 'OfCategory'});
        Product.belongsTo(models.Brand, {foreignKey: 'OfBrand'});
        Product.hasMany(models.UniversalDiscount, {as: 'UniversalDiscounts', foreignKey: 'OfProduct'});
        Product.hasMany(models.OrderDetail, {as: 'OrderDetails', foreignKey: 'OfProduct'});
        Product.hasMany(models.InvoiceDetail, {as : 'InvoiceDetails', foreignKey: 'OfProduct'});
        Product.hasMany(models.VendorProduct, {as: 'VendorProducts', foreignKey: 'OfProduct'});
        Product.hasMany(models.ProductTag, {as: 'ProductTags', foreignKey: 'OfProduct'});
    };
    return Product;
};
