module.exports = (sequelize, DataTypes) => {
    const VendorProduct = sequelize.define('VendorProduct', {
        supplierPrice : {
            type: DataTypes.DOUBLE,
            allowNull: false
        },
        SellingPrice : {
            type: DataTypes.DOUBLE,
            allowNull: false
        },
        Quantity : {
            type: DataTypes.DOUBLE,
            allowNull: false
        },
        availability:{
            type: DataTypes.BOOLEAN
        }
    });

    VendorProduct.associate = models => {
        VendorProduct.belongsTo(models.Product, {as: 'Products', foreignKey: 'OfProduct'});
        VendorProduct.belongsTo(models.Vendor, {as: 'Vendors', foreignKey: 'OfVendor'});
        VendorProduct.belongsTo(models.Variant, {foreignKey: 'OfVarient'});
        VendorProduct.hasMany(models.UniversalDiscount, {as: 'UniversalDiscounts', foreignKey: 'OfVendorProduct'});
    };
    return VendorProduct;
};
