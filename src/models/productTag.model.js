module.exports = (sequelize, DataTypes) => {
    const ProductTag = sequelize.define('ProductTag', {});
    ProductTag.associate = models => {
        ProductTag.belongsTo(models.Tag, { foreignKey: "OfTag"});
        ProductTag.belongsTo(models.Product, { foreignKey: "OfProduct"});
    };

    return ProductTag;
};