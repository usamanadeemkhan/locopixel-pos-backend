module.exports = (sequelize, DataTypes) => {
    const Invoice = sequelize.define('Invoice', {
        discount: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        amount: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        grossAmount: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        dateOfInvoice: {
            type: DataTypes.DATE,

        },

    });

    Invoice .associate = models => {
        Invoice.belongsTo(models.User, {foreignKey: 'OfCustomer'});
        Invoice.belongsTo(models.User, { foreignKey: 'OfUser'});
        Invoice.hasMany(models.InvoiceDetail, {as : 'InvoiceDetails', foreignKey: 'OfInvoice'})
    };
    return Invoice;
};