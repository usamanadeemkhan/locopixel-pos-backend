module.exports = (sequelize, DataTypes) => {
    const Role = sequelize.define('Role', {
        name: {
            type: DataTypes.STRING,
            allowNull: true
        },
        description: {
            type: DataTypes.STRING,
            allowNull: true
        },
    });
    Role.associate = models => {
        Role.hasMany(models.UserRole, {as: 'UserRoles', foreignKey: 'OfRole', onDelete: 'cascade'});
        Role.hasMany(models.RolePermission, {as: 'RolePermissions', foreignKey: 'OfRole', onDelete: 'cascade'});
    };
    return Role;
};
