module.exports = (sequelize, DataTypes) => {
    const Order = sequelize.define('Order', {
        name: {
            type: DataTypes.STRING,
            allowNull: true
        },
        amount: {
            type: DataTypes.DOUBLE,
            allowNull: true
        },
        grossAmount: {
            type: DataTypes.DOUBLE,
            allowNull: true
        },
        expectedDate: {
            type: DataTypes.DATE,

        },
        status: {
            type: DataTypes.STRING,
            allowNull: true
        },
        dateOfOrder: {
            type: DataTypes.DATE,

        },
        receivedDate: {
            type: DataTypes.DATE,

        },
    });

    Order.associate = models => {
        Order.belongsTo(models.User, {as: 'Users', foreignKey: 'OfUser'});
        Order.belongsTo(models.Vendor, {as: 'Vendors', foreignKey: 'OfVendor'});
        Order.belongsTo(models.Outlet, {foreignKey: 'OfOutlet'});
        Order.hasMany(models.OrderDetail, {as : 'OrderDetails', foreignKey: 'OfOrder', onDelete: 'cascade'})
    };
    return Order;
};