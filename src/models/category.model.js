module.exports = (sequelize, DataTypes) => {
    const Category = sequelize.define('Category', {
        name: DataTypes.STRING,
        status: DataTypes.ENUM('Active', 'Deactive'),
        description: DataTypes.STRING,
        isDeleted: DataTypes.BOOLEAN,
    });
    Category.associate = models => {
        Category.hasMany(models.Product, {as: 'Products', foreignKey: 'OfCategory'});
        Category.hasMany(models.Category, {as: 'Categories', foreignKey: 'OfCategory'});
        Category.belongsTo(models.Category, {foreignKey: 'OfCategory'});
        Category.belongsTo(models.Outlet, {foreignKey: 'OfOutlet'});
    };
    return Category;
};