module.exports = (sequelize, DataTypes) => {
    const Outlet = sequelize.define('Outlet', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        location: {
            type: DataTypes.STRING,
            allowNull: true
        },
        timezone: {
            type: DataTypes.DATE,
            allowNull: true
        },
        currency: {
            type: DataTypes.STRING,
            allowNull: true
        },
        phone: {
            type: DataTypes.STRING,
            allowNull: true
        },
        tax: {
            type: DataTypes.DOUBLE,
            allowNull: true
        },
        city: {
            type: DataTypes.STRING,
            allowNull: true
        },
        // completedBatches: { type: DataTypes.INTEGER }
    });

    Outlet.associate = models => {
        Outlet.belongsTo(models.Store, {foreignKey: 'OfStore'});
        Outlet.hasMany(models.Product, {as: 'Products', foreignKey: 'OfOutlet', onDelete: 'cascade'});
        Outlet.hasMany(models.Order, {as: 'Orders', foreignKey: 'OfOutlet', onDelete: 'cascade'});
        Outlet.hasMany(models.OutletGroup, {as: 'OutletGroups', foreignKey: 'OfOutlet', onDelete: 'cascade'});
        Outlet.hasMany(models.UniversalDiscount, {as: 'UniversalDiscounts', foreignKey: 'OfOutlet', onDelete: 'cascade'});
        Outlet.hasMany(models.TaxOutlet, {as: 'TaxOutlets', foreignKey: 'OfOutlet', onDelete: 'cascade'});
        Outlet.hasMany(models.OutletUser, {as: 'OutletUsers', foreignKey: 'OfOutlet', onDelete: 'cascade'});
        Outlet.hasMany(models.Vendor, {as: 'Vendors', foreignKey: 'OfOutlet', onDelete: 'cascade'});
        Outlet.hasMany(models.Category, {as: 'Categorys', foreignKey: 'OfOutlet', onDelete: 'cascade'});
        Outlet.hasMany(models.Brand, {as: 'Brands', foreignKey: 'OfOutlet', onDelete: 'cascade'});
    };
    return Outlet;
};
