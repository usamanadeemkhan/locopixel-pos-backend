const express = require('express');
const controller = require('../controllers/vendor.controller');


const router = express.Router();

router.route('')
    .get(controller.getVendors)
    .post(controller.createVendor);
router.route('/:vendorId')
    .patch(controller.updateVendor)
    .delete(controller.deleteVendor);
router.route('/outlets/:outletId')
    .get(controller.getVendorsofOutlets);
router.route('/:brandId/:categoryId')
    .get(controller.getSpecificSupplierOfOutlet);
module.exports = router;