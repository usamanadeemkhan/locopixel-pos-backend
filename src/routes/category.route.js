const express = require('express');
const controller = require('../controllers/productCategory.controller');


const router = express.Router();

router.route('')
    .get(controller.getCategory)
    .post(controller.createCategory);/*
router.route('/:categoryId')
    .patch(controller.updateStore)
    .delete(controller.deleteStore);*/
router.route('/:outletId')
    .get(controller.getCategoryinOutlet);
module.exports = router;