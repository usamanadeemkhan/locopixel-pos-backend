const express = require('express');
const controller = require('../controllers/role.controller');


const router = express.Router();

router.route('')
    .get(controller.getRoles);
/*.post(controller.createStore); */
router.route('/:roleId')
    .delete(controller.deleteRole);
/* .patch(controller.updateStore)
.delete(controller.deleteStore);
*/
router.route('/permissions')
    .get(controller.getPermission)
    .post(controller.createRole);
module.exports = router;