var express=require('express');
var groupController=require('../controllers/group.controller')
const router=express.Router();


router.route('')
    .get(groupController.getAllGroups)
    .post(groupController.createGroup);

router.route('/:groupId')
    .patch(groupController.updateGroup)
    .delete(groupController.deleteGroup);

module.exports=router;