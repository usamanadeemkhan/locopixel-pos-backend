const express = require('express');
const controller = require('../controllers/transaction.controller');


const router = express.Router();

router.route('')
    .get(controller.getTransactions)
    .post(controller.createTransaction);
router.route('/:invoiceId')
    .patch(controller.updateTransactions)
    .delete(controller.deleteTransaction);

module.exports = router;