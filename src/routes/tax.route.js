const express = require('express');
const controller = require('../controllers/tax.controller');


const router = express.Router();

router.route('')
    .get(controller.getTaxes)
    .post(controller.createTax);
router.route('/taxoutlets/:taxOutletId')
    .get(controller.getOutletTaxes);
/* router.route('/:storeId')
    .patch(controller.updateStore)
    .delete(controller.deleteStore);
*/

module.exports = router;