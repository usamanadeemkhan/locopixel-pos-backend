const express = require('express');
const controller = require('../controllers/store.controller');


const router = express.Router();

router.route('')
    .get(controller.getStores)
    .post(controller.createStore);
router.route('/:storeId')
    .patch(controller.updateStore)
    .delete(controller.deleteStore);

module.exports = router;