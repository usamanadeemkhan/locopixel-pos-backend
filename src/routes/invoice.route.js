const express = require('express');
const controller = require('../controllers/invoice.controller');


const router = express.Router();

router.route('')
    .get(controller.getInvoices)
    .post(controller.createInvoice);
router.route('/:invoiceId')
    .patch(controller.updateInvoice)
    .delete(controller.deleteInvoice);
router.route('/outlet/:outletId')
    .get(controller.getInvoices)
module.exports = router;