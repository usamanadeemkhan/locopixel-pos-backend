const express = require('express');
const controller = require('../controllers/order.controller');


const router = express.Router();

router.route('')
    .get(controller.getAllOrders)
    .post(controller.createOrder);
router.route('/:orderId')
    .patch(controller.updateOder)
router.route('/detail/:orderId')
    .get(controller.getOrderDetail)
router.route('/outlets/:outletId')
    .get(controller.getAllOrdersOfOutlet)
module.exports = router;