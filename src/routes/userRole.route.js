const express = require('express');
const controller = require('../controllers/userRole.controller');


const router = express.Router();

router.route('')
    .get(controller.getRoles)
    .post(controller.createRole);
router.route('/:roleId')
    .patch(controller.updateRole)
    .delete(controller.deleteRole);

module.exports = router;