const express = require('express');
const controller = require('../controllers/product.controller');


const router = express.Router();

router.route('')
    .get(controller.getAllProducts)
    .post(controller.createProduct);
router.route('/:categoryId')
    .patch(controller.updateProduct)
    .delete(controller.deleteProduct);
router.route('/:outletId')
    .get(controller.getAllProductsforDiscount);
router.route('/outlets/:outletId')
    .get(controller.getAllProductsofOutlets);
router.route('/vendor-product/:vendorId/:outletId')
    .get(controller.getallproductsforoutletbyVendors);
module.exports = router;