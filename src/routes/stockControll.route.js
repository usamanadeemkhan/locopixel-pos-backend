const express = require('express');
const controller = require('../controllers/stockControll.controller');


const router = express.Router();

router.route('/:outletId')
    .get(controller.getStockProducts)
module.exports = router;