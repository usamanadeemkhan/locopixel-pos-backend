const express = require('express');
const controller = require('../controllers/outlet.controller');


const router = express.Router();

router.route('/:storeId')
    .get(controller.getAllOutlets)
    .post(controller.createOutlet);
router.route('/:storeId/:outletId')
    .patch(controller.updateOutlet)
    .delete(controller.deleteOutLet);

module.exports = router;