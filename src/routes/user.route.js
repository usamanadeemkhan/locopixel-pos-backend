const express = require('express');
const validate = require('express-validation');
const controller = require('../controllers/user.controller');


const router = express.Router();

router.route('/login')
    .post(controller.login);

router.route('/employees')
    .get(controller.getAllUsers)
    .post(controller.createUser);
router.route('/employees/:userId')
    .patch(controller.updateUser)
    .delete(controller.deleteUser);

router.route('/customers')
    .get(controller.getAllCustomers)
    .post(controller.createCustomer);
router.route('/customers/:customerId')
    .patch(controller.updateCustomer)
    .delete(controller.deleteCustomer);

module.exports = router;