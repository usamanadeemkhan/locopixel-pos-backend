var express=require('express');
var discountController=require('../controllers/discount.controller');
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
const router=express.Router();


router.route('/')
    .get(discountController.getAllDiscount)
    .post(discountController.addCustomerBasedDiscount);

router.route('/:discountName')
    .patch(discountController.updateDiscount)
    .delete(discountController.deleteDiscount);

module.exports=router;