const express = require('express');
const controller = require('../controllers/brand.controller');
const router = express.Router();
router.route('')
    .get(controller.getBrand)
    .post(controller.createBrand);
router.route('/:categoryId')
    .patch(controller.updateBrand)
    .delete(controller.deleteBrand);
router.route('/:outletId')
    .get(controller.getBrandofOutlet);
module.exports = router;