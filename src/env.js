var config = require('../config/config');

const env = process.env.NODE_ENV || 'development';
const ENV = config[env];

module.exports = ENV;