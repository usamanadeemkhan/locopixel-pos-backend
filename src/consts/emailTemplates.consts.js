const emailTemplates = {
    REGISTRATION: Symbol('REGISTRATION'),
    RESETPASSWORD: Symbol('RESETPASSWORD'),
    NEWSLETTER: Symbol('NEWSLETTER')
};

module.exports = emailTemplates;