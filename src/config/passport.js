var Strategy = require('passport-jwt');
var BearerStrategy = require('passport-http-bearer');
var authProviders = require('../services/authProviders');
var ENV = require('../env');

const jwtOptions = {
    secretOrKey: ENV['jwtSecret'],
    jwtFromRequest: Strategy.ExtractJwt.fromAuthHeaderWithScheme('Bearer')
};

const jwt = async (payload, done) => {
    try {
        // const user = await User.findById(payload.sub);
        const user = payload;
        if (user) return done(null, user);
        return done(null, false);
    } catch (error) {
        return done(error, false);
    }
};

const oAuth = service => async (token, done) => {
    try {
        const userData = await authProviders[service](token);
        // const user = await User.oAuthLogin(userData);
        return done(null, user);
    } catch (err) {
        return done(err);
    }
};

//exports.jwt = new Strategy.ExtractJwt(jwtOptions, jwt);
exports.facebook = new BearerStrategy(oAuth('facebook'));
exports.google = new BearerStrategy(oAuth('google'));
