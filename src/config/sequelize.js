var dbSeeder = require("./dbSeeder");
var logger = require("./logger");
var fs = require("fs");
var path = require("path");
var Sequelize = require("sequelize");
var ENV = require("../env");

const basename = path.basename(module.filename);
const db = {};

let sequelize = new Sequelize('test_schema', 'root', 'password',
    {
        dialect: 'mysql',
        port: '3306',
        host: 'localhost',
        operatorsAliases: false,
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        }
    });

let sourcePath = path.normalize(__dirname + '/..');
sourcePath += '/models';

fs.readdirSync(sourcePath)
    .filter(file =>
        (file.indexOf('.') !== 0) &&
        (file !== basename) &&
        (file.slice(-3) === '.js'))
    .forEach(file => {
        const model = sequelize.import(path.join(sourcePath + '/' + file));
        db[model.name] = model;
    });

sequelize.sync({
    force: ENV.dbSync,
    logging: ENV.dbLog
}).then(() => {
    if (ENV.dbSync) {
        dbSeeder(db, sequelize);
        logger.log({ sync: 'Sync is ON' });
    }
    logger.log({ info: 'Database has been Connected.' });
}).catch(err => logger.log({ err }));

Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;