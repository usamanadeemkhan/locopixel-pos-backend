var ENV = require('../env');
var bcrypt = require('bcryptjs');


module.exports = (db, sequelize) => {
    const createSeed = async () => {
        console.log('Seeder');

        // Role 1
        /*const createUserRole1 = await db.UserRole.create({
            roleName: 'Admin',
            roleDescription: 'Creates stores, manages outlets , blah blahh...'
        });*/

        // User 1
        const createUser1 = await db.User.create({
            email: "admin@locopixel.com",
            name: "Amir Liaqat",
            dateOfBirth: "1997-03-26 06:56:13",
            password: await bcrypt.hash('POSTest', ENV['CRYPT_ROUNDS']),
            phoneNumber: "+923 xxx-xxx-xxx",
            gender: "Male",
            address: "Admin Address",
            country: "Pakistan",
            city: "Islamabad",
            isAdmin: true
        });

        // Role 1 + User 2
        const createdRole1 = await db.Role.create({
            name: 'Manager',
            description: 'Can manage everything'
        });
        const createUser2 = await db.User.create({
            email: "manager@locopixel.com",
            name: "Pasha",
            dateOfBirth: "2000-06-26 06:56:13",
            password: await bcrypt.hash('POSTest', ENV['CRYPT_ROUNDS']),
            phoneNumber: "+923 xxx-xxx-xxx",
            gender: "Male",
            address: "Customer Address",
            country: "Pakistan",
            city: "Islamabad",
            orderTotal: "153",
            spentTotal: "50",
            visitTotal: "31",
            isAdmin: false
        });
        await db.UserRole.create({
            OfUser: createUser2.id,
            OfRole: createdRole1.id
        });

        // Permission 1
        const createdPermission1 = await db.Permission.create({
            type: "Sales",
            canEdit: 1,
            canView: 1
        });

        // Permission Role 1
        await db.RolePermission.create({
            OfPermission: createdPermission1.id,
            OfRole: createdRole1.id
        });

        // Discount 1
        const createDiscount1 = await db.Discount.create({
            name: "Ramzan Offer",
            description: "Huge Sale on this Ramzan",
            type: "Product Based",
            percentage: 50,
            status: "Active",
            startDate: "2018-05-17",
            endDate: "2018-06-16",
        });

        // Brand 1
        const createBrand1 = await db.Brand.create({
            name: "CocaCola"
        });

        // Category 1
        const createCategory1 = await db.Category.create({
            name: "Cold Drink",
            image: "",
            status: "Active",
            isDeleted: false
        });

        // Store 1
        const createStore1 = await db.Store.create({
            name: "Alif baba",
            noOfOutlet: 2,
            OfUser: createUser1.id,
        });

        const createOutlet1 = await db.Outlet.create({
            name: "Tot batot",
            location: "F11",
            timezone: "2018-01-02",
            currency: "PKR",
            tax: "20.5",
            city: "Islamabad",
            OfStore: createStore1.id,
            OfUser: createUser1.id,
        });

        // Group 1
        await db.Group.create({
            name: "VIP",
            description: "Elite group members",
            OfDiscount: createDiscount1.id,
            OfOutlet: createOutlet1.id
        });

        // Product 1
        const createProduct1 = await db.Product.create({
            name: "Coca cola",
            description: "Coca  cola tu",
            supplyPrice: 30,
            retailPrice: 35,
            markup: 20,
            totalPrice: 50.00,
            productType: "Single",
            barcode: 8921301,
            image: "",
            sku: 2123,
            batchNumber: 3,
            inHandQuantity: 1253,
            minimumQuantity: 150,
            expiryDate: "2021-12-12",
            OfBrand: createBrand1.id,
            OfCategory: createCategory1.id,
            OfDiscount: createDiscount1.id,
            OfOutlet: createOutlet1.id
        });

        // Tags 1
        const createTag1 = await db.Tag.create({
            name: "Tag1"
        });

        // Product Tags1
        await db.ProductTag.create({
            OfProduct: createProduct1.id,
            OfTag: createTag1.id
        });
        // Invoice 1
        const createInvoices1 = await db.Invoice.create({
            discount: 20,
            amount: 300,
            grossAmount: 240,
            OfCustomer: createUser2.id
        });

        await db.InvoiceDetail.create({
            quantity: 20,
            price: 20,
            amount: 340,
            discount: 20,
            dateOfOrder: "2018-01-02",
            grossAmount: 4010,
            OfInvoice: createInvoices1.id,
            OfProduct: createProduct1.id
        });

        console.log("DB Seed Has been Completed");

    };
    createSeed().then();
};