const sgMail = require('@sendgrid/mail');
const emailTemplates = require('../consts/emailTemplates.consts');
const ejs = require('ejs');
const fs = require('fs');
const ENV = require('../env');
const templates = {};

templates[emailTemplates.REGISTRATION]  = fs.readFileSync(__dirname + '/emailTemplates/register.ejs', 'ascii');
templates[emailTemplates.RESETPASSWORD] = fs.readFileSync(__dirname + '/emailTemplates/resetPassword.ejs', 'ascii');
templates[emailTemplates.NEWSLETTER]    = fs.readFileSync(__dirname + '/emailTemplates/newsLetter.ejs', 'ascii');
sgMail.setApiKey(ENV['EMAIL']['API_KEY']);

const sendEmail = (to, subject, text, html) => {
    const msg = {
        to: to,
        from: ENV['EMAIL']['SENDER'],
        fromname: 'AGRIDept',
        subject: subject,
        text: text,
        html: html,
    };
    return sgMail.send(msg);
};

// const sendRegistrationEmail = (to, key, name) =>  {
//     const rendered = ejs.render(templates[emailTemplates.REGISTRATION], { name: name, link:  `${ENV['SERVICE_URL']}auth/verify-email?email=${to}&key=${key}` });
//     return sendEmail(to, 'Registration - Verify Email', 'Your browser does not support this Email', rendered);
// };

const resetPasswordEmail = (to, key, name) => {
    const rendered = ejs.render(templates[emailTemplates.RESETPASSWORD], { name: name, link:  `${ENV['SERVICE_URL']}/reset?email=${to}&key=${key}` });
    return sendEmail(to, 'Reset Password', 'Your browser does not support this Email', rendered);
};

module.exports = { sendEmail, resetPasswordEmail };