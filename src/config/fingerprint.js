const Fingerprint = require('express-fingerprint');

module.exports = Fingerprint({
    parameters:[
        Fingerprint.useragent,
        Fingerprint.acceptHeaders,
        Fingerprint.geoip
    ]
});
