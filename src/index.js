var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
var bodyParser = require('body-parser');
var compress = require('compression');
var methodOverride = require ('method-override');
var helmet = require('helmet');
var passport = require('passport');
var strategies = require('./config/passport');
var fingerPrint = require('./config/fingerprint');
var logger = require('./config/logger');

const rootPath = path.normalize(__dirname + '/..');

/**
 * Express instance
 * @public
 */
const app = express();

// request logging. production || combined || dev
app.use(morgan(ENV['log_type']));

// parse body params and attache them to req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// gzip compression
app.use(compress());

// lets you use HTTP verbs such as PUT or DELETE
// in places where the client doesn't support it
app.use(methodOverride());

// secure apps by setting various HTTP headers
app.use(helmet());

// enable CORS - Cross Origin Resource Sharing
app.use(cors());
app.options('*', cors());

app.use(fingerPrint);

// enable authentication
app.use(passport.initialize());
passport.use('jwt', strategies.jwt);
passport.use('facebook', strategies.facebook);
passport.use('google', strategies.google);

// Health API
app.get('/api/health', (req, res) => res.send({ status: 'OK', message: 'API updated on 8 may 2:am' }));

app.get('/api/fingerprint', (req, res) => res.send(req.fingerprint));

// Add routes
app.use('/api/v1', routes);

app.all('*', (req, res) => {
    res.sendFile(rootPath + '/public/index.html');
});

// if error is not an instanceOf APIError, convert it.
app.use(error.converter);

// catch 404 and forward to error handler
app.use(error.notFound);