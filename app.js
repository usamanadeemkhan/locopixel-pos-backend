var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
var bodyParser = require('body-parser');
var compress = require('compression');
var methodOverride = require('method-override');
var helmet = require('helmet');
//var passport = require('passport');
//var strategies = require('./config/passport');
//var fingerPrint require('./config/fingerprint');
//var logger = require('./config/logger');

var crawl = require('./src/controllers/my Crawlwer.js');
var users = require('./src/routes/user.route');
var stores = require('./src/routes/store.route');
var vendors = require('./src/routes/vendor.route');
var orders = require('./src/routes/order.route');
var categories = require('./src/routes/category.route');
var outlets = require('./src/routes/outlet.route');
var discounts = require('./src/routes/discount.route');
var groups = require('./src/routes/group.route');
var userRoles = require('./src/routes/userRole.route');
var products = require('./src/routes/product.route');
var salesLedger = require('./src/routes/salesLedger.route');
var roles = require('./src/routes/role.route');
var brands = require('./src/routes/brand.route');
var invoices = require('./src/routes/invoice.route');
var taxes = require('./src/routes/tax.route');
var stockControll = require('./src/routes/stockControll.route');

var indexRouter = require('./src/routes/index');
var app = express();
// view engine setup

app.use(cors());
app.options('*', cors());


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', indexRouter);


app.use('/users', users);
app.use('/stores', stores);
app.use('/vendors', vendors);
app.use('/orders', orders);
app.use('/categories', categories);
app.use('/outlets', outlets);
app.use('/discounts', discounts);
app.use('/groups', groups);
app.use('/userroles', userRoles);
app.use('/roles', roles);
app.use('/products', products);
app.use('/sales-ledger', salesLedger);
app.use('/discounts', discounts);
app.use('/brands', brands);
app.use('/invoice', invoices);
app.use('/taxes', taxes);
app.use('/stockControll', stockControll);
// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));

});
// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    // render the error page
    res.status(err.status || 500);
    res.render('error');
});
app.listen(5000);
module.exports = app;

/*
const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const path = require('path');
const bodyParser = require('body-parser');
const compress = require('compression');
const methodOverride = require ('method-override');
const helmet = require('helmet');
const passport = require('passport');
const strategies = require('./src/config/passport');
const fingerPrint = require('./src/config/fingerprint');
const logger = require('./src/config/logger');

const getUser = require('./src/routes/user.route');

const routes = require('./src/routes');
const error = require('./src/middlewares/error');
const ENV = require('./src/env');

const rootPath = path.normalize(__dirname + '/');

/!**
 * Express instance
 * @public
 *!/
const app = express();

app.use('/user', getUser);
// request logging. production || combined || dev
app.use(morgan(ENV['log_type']));

// parse body params and attache them to req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// gzip compression
app.use(compress());

// lets you use HTTP verbs such as PUT or DELETE
// in places where the client doesn't support it
app.use(methodOverride());

// secure apps by setting various HTTP headers
app.use(helmet());

// enable CORS - Cross Origin Resource Sharing
app.use(cors());
app.options('*', cors());

app.use(fingerPrint);

// enable authentication
app.use(passport.initialize());
//passport.use('jwt', strategies.jwt);
passport.use('facebook', strategies.facebook);
passport.use('google', strategies.google);


app.all('*', (req, res) => {
    res.sendFile(rootPath + '/public/index.html');
});

// if error is not an instanceOf APIError, convert it.
app.use(error.converter);

// catch 404 and forward to error handler
app.use(error.notFound);

// error handler, send stacktrace only during development
app.use(error.handler);

app.listen(3001);

module.exports = app;*/
